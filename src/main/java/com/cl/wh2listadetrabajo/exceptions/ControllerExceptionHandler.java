package com.cl.wh2listadetrabajo.exceptions;

import com.cl.wh2listadetrabajo.error.Error;
import com.cl.wh2listadetrabajo.error.ErrorCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import static com.cl.wh2listadetrabajo.util.Constants.LOG_ERROR;

@RestControllerAdvice
public class ControllerExceptionHandler {

    private final Logger logger = LoggerFactory.getLogger(ControllerExceptionHandler.class);

    @ExceptionHandler(value = CustomException.class)
    public ResponseEntity<Error> handleCustomException(CustomException ex){
        logger.error(LOG_ERROR, ex.getMessage(), ex);
        var error = new Error();
        error.setIndex(ex.getIndex());
        error.setCode(ex.getErrorCode());
        error.setDetail(ex.getDetail());
        return ResponseEntity.status(ex.getErrorCode().getStatus()).body(error);
    }

    @ExceptionHandler(value = Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public Error handleException(Exception ex){
        logger.error(LOG_ERROR, ex.getMessage(), ex);
        ErrorCode code = ErrorCode.INTERNAL_ERROR;
        var error = new Error();
        error.setIndex(code.getIndex());
        error.setCode(code);
        error.setDetail(code.getMsg());
        return error;
    }

}
