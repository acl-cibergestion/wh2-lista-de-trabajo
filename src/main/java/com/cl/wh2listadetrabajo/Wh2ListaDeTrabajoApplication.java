package com.cl.wh2listadetrabajo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class Wh2ListaDeTrabajoApplication {

	public static void main(String[] args) {
		SpringApplication.run(Wh2ListaDeTrabajoApplication.class, args);
	}

	
	@Bean
	public RestTemplate getRestTemplate() {
		return new RestTemplate();
	}
}
