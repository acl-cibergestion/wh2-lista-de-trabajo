package com.cl.wh2listadetrabajo.especificaciones;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Root;
import java.util.Collection;

/**
 * El primer parámetro (root) le permite seleccionar el campo por el que desea filtrar.
 * Esto se puede hacer usando root.get ("nombre")
 * o si necesita unirse, puede usar root.join ("myField").
 * El segundo parámetro (query) no se usa con tanta frecuencia,
 * pero contiene información sobre el tipo de consulta que se está ejecutando.
 * El último parámetro es cb, que le permite definir exactamente qué tipo de consulta
 * desea construir (LIKE, IS NULL, CONTAINS, AND, OR, =,…).
 */

@Service
public final class SpecificationsBuilder<T> {

    public Specification<T> withParameterOfAnotherEntity(String param, String column, String parameterOfAnotherEntity) {
        if (param == null) {
            return null;
        } else {
            return (root, query, cb) -> cb.equal(root.join(column).get(parameterOfAnotherEntity), param);
        }
    }

    public Specification<T> withParameterOfTheEntityItself(String param, String column) {
        if (param == null) {
            return null;
        } else {
            return (root, query, cb) -> cb.equal(root.get(column), param);
        }
    }

    public Specification<T> withParameterIsNull(T param, T column) {
        if (param != null && param.equals("true")) {
            return (root, query, cb) -> cb.isNull(root.get((String) column));
        } else {
            return null;
        }
    }

    public Specification<T> withInnerJoinWithOtherTable(T tablet,T columnRelation,T fieldTabletRelation,T param) {
        if (param == null) {
            return null;
        } else {
            return (root, query, cb) -> {
                Root<?> tabletRelation = query.from(tablet.getClass());
                Expression<Collection<T>> lookUpTable = tabletRelation.get((String) columnRelation);
                return cb.and(cb.equal(tabletRelation.get((String) fieldTabletRelation), param), cb.isMember(root, lookUpTable));
            };
        }
    }

}

