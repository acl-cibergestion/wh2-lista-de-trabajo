package com.cl.wh2listadetrabajo.servicios;

import com.cl.wh2listadetrabajo.dtos.*;
import com.cl.wh2listadetrabajo.exceptions.CustomException;
import com.cl.wh2listadetrabajo.model.*;

import java.io.IOException;
import java.util.List;


public interface IListaDeTrabajoService {
	
	CarpetaResponse findCarpeta(String qparams) throws IOException;

	List<Sucursal> findSucursales(String qparams) throws CustomException;

	List<FilterFlujoCanal> findFlujoCanalByCarpeta(String qParams) throws CustomException;

	List<DestinoCredito> findDestinos(String qparams) throws CustomException;

	List<Segmento> findSegmentos(String qparams) throws CustomException;

	List<String> findActividades(String qparams) throws CustomException;

	PaginadoResponse findListaTrabajo(String qParams) throws CustomException;

	List<HistoricoResponse> findHistoricoActividades(String qparams) throws CustomException;

	String findActividad(String qparams) throws CustomException;

	List<TaskDTO> findListActividades(String qparams) throws CustomException;

	PerfilamientoReasignar findByInstitucion(String qparams) throws CustomException;

	void findAsistidos(UsuarioBd usuario, List<Usuario> responsables);

	List<Region> findRegiones(String qparams) throws CustomException;

	ListaTrabajoFiltros findListaTrabajoFilters(String qParams) throws CustomException;

	List<Usuario> findResponsables(String qparams) throws CustomException;

	TareasPendientesResponse findTareasPendientes(String qparams) throws CustomException;

	Boolean validarTieneFormulario(String qparams) throws CustomException;

	List<TareasAsignadasResponseDTO> cargarTareasAsignadas(String qparams) throws CustomException;
}
