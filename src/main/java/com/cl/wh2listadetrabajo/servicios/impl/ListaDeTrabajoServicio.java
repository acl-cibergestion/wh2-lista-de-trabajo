package com.cl.wh2listadetrabajo.servicios.impl;


import com.cl.wh2listadetrabajo.dtos.*;
import com.cl.wh2listadetrabajo.dtos.EstadoCarpeta;
import com.cl.wh2listadetrabajo.error.ErrorCode;
import com.cl.wh2listadetrabajo.error.UtilJson;
import com.cl.wh2listadetrabajo.especificaciones.SpecificationsBuilder;
import com.cl.wh2listadetrabajo.exceptions.CustomException;
import com.cl.wh2listadetrabajo.model.Canal;
import com.cl.wh2listadetrabajo.model.Carpeta;
import com.cl.wh2listadetrabajo.model.Flujo;
import com.cl.wh2listadetrabajo.model.*;
import com.cl.wh2listadetrabajo.model.util.DividerJsonParameters;
import com.cl.wh2listadetrabajo.repositorios.*;
import com.cl.wh2listadetrabajo.servicios.IListaDeTrabajoService;
import com.cl.wh2listadetrabajo.util.Constants;
import com.fasterxml.jackson.core.JsonProcessingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.*;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.Predicate;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import static com.cl.wh2listadetrabajo.util.Constants.*;

@Service
public class ListaDeTrabajoServicio implements IListaDeTrabajoService {

    private final Logger logger = LoggerFactory.getLogger(ListaDeTrabajoServicio.class);

    private static final String FUERA_ESTANDAR = "0";
    private static final String DENTRO_ESTANDAR = "1";
    private static final String DENTRO_ESTANDAR_LIMITE = "2";
    private static final String DATE_FORMAT = "dd/MM/yyyy HH:mm";

    @Value("${wh2.minio}")
    String minioUrl;

    @Value("${ciber.gestion.bucket.minio}")
    String bucketName;

    @Value("${minio.file.link.duration}")
    String fileLinkDuration;

    @Autowired
    private UsuarioRepositorio usuarioRepositorio;

    @Autowired
    private DestinoCreditoRepositorio destinoCreditoRepositorio;

    @Autowired
    private SegmentoRepositorio segmentoRepositorio;

    @Autowired
    private HistoryTaskRepositorio historyTaskRepositorio;

    @Autowired
    private HistoryTaskRestoSearchRepositorio historyTaskRestoSearchRepositorio;

    @Autowired
    private CarpetaRepositorio carpetaRepositorio;

    @Autowired
    private FoContabilizacionRepositorio foContabilizacionRepositorio;

    @Autowired
    private PerfilamientoReasignarRepositorio perfilamientoReasignarRepositorio;

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    SpecificationsBuilder<HistoryTaskResto> specificationBuilder;
    
    @Autowired
    SpecificationsBuilder<PerfilamientoReasignar> specificationBuilderReasignar;

    @Autowired
    SpecificationsBuilder<HistoryTask> specificationsBuilder;

    @Autowired
    SpecificationsBuilder<FoChecklist> specificationsBuilderCheckList;

    @Autowired
    private HistoryTaskRestoRepositorio historyTaskRestoRepositorio;

    @Autowired
    private SucursalBancoRepositorio sucursalBancoRepositorio;

    @Autowired
    private RegionRepositorio regionRepositorio;

    @Autowired
    private FlujoRepositorio flujoRepositorio;

    @Autowired
    private CanalRepositorio canalRepositorio;
    
    @Autowired
    private CustomTaskDefinitionRepositorio customTaskDefinitionRepositorio;
    
    @Autowired
    private RunningTaskRestoRepositorio runningTaskRestoRepositorio;
    
    @Autowired
    private CaCreditoRepositorio caCreditoRepositorio;

    @Autowired
    private AsistenteRepositorio asistenteRepositorio;

    @Override
    public CarpetaResponse findCarpeta(String qparams) throws IOException {

        CarpetaResponse carpeta = null;
        var json = UtilJson.getValues(qparams);
        Map<String, String> filters = DividerJsonParameters.getFilters(json, "filters");
        Object value = filters.get("id");
        var carpetaId = Long.valueOf((Integer) value);
        Optional<Carpeta> opCarpeta = carpetaRepositorio.findById(carpetaId);
        if (opCarpeta.isPresent()) {
            carpeta = mapper(opCarpeta.get());
        }

        List<CaCredito> creditos = caCreditoRepositorio.findByCarpetaId(carpetaId);
        if(!creditos.isEmpty() && Objects.nonNull(carpeta)) {
        	carpeta.setCredito(mapperCredito(creditos.get(0)));
        }
        
        if (Objects.nonNull(carpeta)) {
            var contabilizacion = foContabilizacionRepositorio.findFoContabilzacionByCarpeta(carpeta.getId());
            if (contabilizacion != null) {
                carpeta.setNumPCF(contabilizacion.getNumeroPcf());
            }
        }

        return carpeta;
    }

    private Cacredito mapperCredito(CaCredito caCreditoDB) {
    	
    	var creditoDto = new Cacredito();
    	creditoDto.setId(caCreditoDB.getId());
    	creditoDto.setNumOperacion(caCreditoDB.getNumOperacion());
    	var notaria = new Usuario();
    	if(Objects.nonNull(caCreditoDB.getNotaria())){
    		notaria.setId(caCreditoDB.getNotaria().getId());
    		notaria.setApellidoPaterno(caCreditoDB.getNotaria().getApellidoPaterno());
    		notaria.setNombreCompleto(getNombreCompletoUsuario(caCreditoDB.getNotaria()));
    		notaria.setNombreUsuario(caCreditoDB.getNotaria().getNombreUsuario());
    		notaria.setPrimerNombre(caCreditoDB.getNotaria().getPrimerNombre());
    		creditoDto.setNotaria(notaria);
    	}
        
		return creditoDto;
	}

	@Override
    public List<Sucursal> findSucursales(String qparams) throws CustomException {

        try {
            List<Sucursal> sucursales = new ArrayList<>();
            var json = UtilJson.getValues(qparams);
            Map<String, String> filters = DividerJsonParameters.getFilters(json, CONST_FILTERS);
            Object value = filters.get(CONST_IDS);
            if (value != null) {
                @SuppressWarnings("unchecked")
                ArrayList<Integer> ids = (ArrayList<Integer>) value;
                for (Integer id : ids) {
                    Optional<Sucursal> opSucursal = sucursalBancoRepositorio.findById(Long.valueOf(id.longValue()));
                    if (opSucursal.isPresent()) {
                        sucursales.add(opSucursal.get());
                    }
                }
            }
            return sucursales;
        } catch (Exception e) {
            throw new CustomException(ErrorCode.INTERNAL_ERROR);
        }
    }

    @Override
    public List<FilterFlujoCanal> findFlujoCanalByCarpeta(String qParams) throws CustomException {


        List<FilterFlujoCanal> flujosCanalesfilters = new ArrayList<>();

        try {
            var json = UtilJson.getValues(qParams);
            Map<String, String> filters = DividerJsonParameters.getFilters(json, CONST_FILTERS);
            String username = filters.get(CONST_USERNAME);
            List<Object[]> flujosCanales = carpetaRepositorio.findDistinctFlujoCanalByUser(username);
            for (Object[] objects : flujosCanales) {
                var canal = (Canal) objects[0];
                var flujo = (Flujo) objects[1];
                flujosCanalesfilters.add(new FilterFlujoCanal(flujo.getId(), canal.getId(), canal.getDescripcion() + " (" + flujo.getDescripcion() + ")"));
            }
        } catch (JsonProcessingException e) {
            throw new CustomException(ErrorCode.INTERNAL_ERROR);
        }
        return flujosCanalesfilters;
    }

    @Override
    public List<DestinoCredito> findDestinos(String qparams) throws CustomException {

        try {
            List<DestinoCredito> destinos = new ArrayList<>();
            var json = UtilJson.getValues(qparams);
            Map<String, String> filters = DividerJsonParameters.getFilters(json, CONST_FILTERS);
            Object value = filters.get(CONST_IDS);
            if (value != null) {
                @SuppressWarnings("unchecked")
                ArrayList<Integer> ids = (ArrayList<Integer>) value;
                for (Integer id : ids) {
                    Optional<DestinoCredito> opDestino = destinoCreditoRepositorio.findById(Long.valueOf(id.longValue()));
                    if (opDestino.isPresent()) {
                        destinos.add(opDestino.get());
                    }
                }
            }
            return destinos;
        } catch (Exception e) {
            throw new CustomException(ErrorCode.INTERNAL_ERROR);
        }
    }

    @Override
    public List<Segmento> findSegmentos(String qparams) throws CustomException {

        try {
            List<Segmento> segmentos = new ArrayList<>();
            var json = UtilJson.getValues(qparams);
            Map<String, String> filters = DividerJsonParameters.getFilters(json, CONST_FILTERS);
            Object value = filters.get(CONST_IDS);
            if (value != null) {
                @SuppressWarnings("unchecked")
                ArrayList<Integer> ids = (ArrayList<Integer>) value;
                for (Integer id : ids) {
                    Optional<Segmento> opSegmento = segmentoRepositorio.findById(Long.valueOf(id.longValue()));
                    if (opSegmento.isPresent()) {
                        segmentos.add(opSegmento.get());
                    }
                }
            }
            return segmentos;
        } catch (Exception e) {
            throw new CustomException(ErrorCode.INTERNAL_ERROR);
        }
    }

    @Override
    public List<String> findActividades(String qparams) throws CustomException {

        try {
            var json = UtilJson.getValues(qparams);
            Map<String, String> filters = DividerJsonParameters.getFilters(json, CONST_FILTERS);
            String username = filters.get(CONST_USERNAME);
            if (username != null) {
                return historyTaskRepositorio.findDistinctHistoryTaskByUser(username);
            }

        } catch (Exception e) {
            throw new CustomException(ErrorCode.INTERNAL_ERROR);
        }
        return Collections.emptyList();
    }

    @Override
    public PaginadoResponse findListaTrabajo(String qParams) throws CustomException {
        try {
            List<ListaTrabajo> listaTrabajos = new ArrayList<>();
            var json = UtilJson.getValues(qParams);
            Map<String, String> filters = DividerJsonParameters.getFilters(json, CONST_FILTERS);
            Map<String, String> paginate = DividerJsonParameters.getFilters(json, "paginate");
            long startTime = System.nanoTime();
            String username = filters.get(CONST_USERNAME);
            var conteoInicial = Boolean.valueOf(filters.get("conteoInicial"));
            UsuarioBd usuario = usuarioRepositorio.findByNombreUsuario(username);
            if (Objects.isNull(usuario)) {
                return new PaginadoResponse(0, listaTrabajos, 0, 0);
            }
            logger.info("usuario ID: {}", usuario.getId());

            Long[] usuariosIds = setUsersId(usuario, conteoInicial);

            var sb = new StringBuilder(Constants.QUERY_LIST_WORK);
            var sbCount = new StringBuilder(Constants.QUERY_COUNT_LIST_WORK);
            var sbCountDistintRut = new StringBuilder(Constants.QUERY_COUNT_RUT_LIST_WORK);

            setFilters(filters, sb, sbCount, sbCountDistintRut, usuario, conteoInicial);

            var query = entityManager.createNativeQuery(sb.toString());
            setUserIdToQuery(query, usuario, conteoInicial, usuariosIds);

            query.setFirstResult(Integer.parseInt(paginate.get(CONST_PAGE)) * Integer.parseInt(paginate.get(CONST_SIZE)));
            query.setMaxResults(Integer.parseInt(paginate.get(CONST_SIZE)));

            var iListaTrabajos = query.getResultList();
            var queryTotal = entityManager.createNativeQuery(sbCount.toString());

            setUserIdToQuery(queryTotal, usuario, conteoInicial, usuariosIds);

            var queryTotalPerson = entityManager.createNativeQuery(sbCountDistintRut.toString());

            setUserIdToQuery(queryTotalPerson, usuario, conteoInicial, usuariosIds);

            int countResult = (Integer) queryTotal.getSingleResult();
            int totalClientes = (Integer) queryTotalPerson.getSingleResult();
            Pageable pageable = PageRequest.of(Integer.parseInt(paginate.get(CONST_PAGE)), Integer.parseInt(paginate.get(CONST_SIZE)));
            Page<Object[]> results = new PageImpl<>(iListaTrabajos, pageable, countResult);

            var totalPages = results.getTotalPages();
            var totalElement = results.getTotalElements();

            results.getContent().forEach(item -> {
                Long numCarpeta = ((BigInteger) item[0]).longValue();
                BigDecimal numPCF = (BigDecimal) item[1];
                Long prioridad = ((BigInteger) item[2]).longValue();
                String motivoPrioridad = (String) item[3];
                String rutResult = (String) item[4];
                String nombreCliente = (String) item[5];
                String apellidoPaterno = (String) item[6];
                String apellidoMaterno = (String) item[7];
                String nombreActividad = (String) item[8];
                var dateFormat = new SimpleDateFormat(DATE_FORMAT);
                String fechaInicio = null;
                if (Objects.nonNull(item[9])) {
                    fechaInicio = dateFormat.format(item[9]);
                }
                String fechaEsperada = null;
                if (Objects.nonNull(item[10])) {
                    fechaEsperada = dateFormat.format(item[10]);
                }
                Integer ee = (Integer) item[11];
                String formularioId = Optional.ofNullable(item[12]).map(Objects::toString).orElse("");
                String checkListId = Optional.ofNullable(item[13]).map(Objects::toString).orElse("");
                String dentroEstandar = calcularEstandar(item[27], item[9], item[10]);
                String actividadId = (String) item[15];
                Long tipoOperacionId = ((BigInteger) item[16]).longValue();
                String runningExecutionId = (String) item[17];
                String taskDefKey = (String) item[18];
                String procDefId = (String) item[19];
                Long canalId = ((BigInteger) item[21]).longValue();
                Long flujoId = ((BigInteger) item[22]).longValue();
                Long sucursalId = null;
                Long regionId = null;
                Long segmentoId = null;
                Long destinoId = null;
                if (Objects.nonNull(item[23]))
                    sucursalId = ((BigInteger) item[23]).longValue();
                if (Objects.nonNull(item[24]))
                    regionId = ((BigInteger) item[24]).longValue();
                if (Objects.nonNull(item[25]))
                    segmentoId = ((BigInteger) item[25]).longValue();
                if (Objects.nonNull(item[26]))
                    destinoId = ((BigInteger) item[26]).longValue();

                listaTrabajos.add(new ListaTrabajo(actividadId, numCarpeta, numPCF.longValue(), prioridad,
                        motivoPrioridad, rutResult, nombreCliente, apellidoPaterno, apellidoMaterno,
                        nombreActividad, fechaInicio, fechaEsperada, ee, dentroEstandar,
                        tipoOperacionId, formularioId, checkListId, runningExecutionId,
                        taskDefKey, procDefId, canalId, flujoId, sucursalId, regionId, segmentoId, destinoId));

            });

            long endTime = System.nanoTime();
            logger.info("segundos: {}", (endTime - startTime));

            String filtroEstandar = filters.get(CONST_ESTANDAR_FILTER);

            return getResponseListaTrabajo(listaTrabajos, filtroEstandar, totalPages, totalElement, totalClientes);

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new CustomException(ErrorCode.INTERNAL_ERROR);
        }
    }

    @Override
    public List<HistoricoResponse> findHistoricoActividades(String qparams) throws CustomException {
        try {
            var json = UtilJson.getValues(qparams);
            Map<String, String> filters = DividerJsonParameters.getFilters(json, CONST_FILTERS);

            //Borrar Paginado luego:
            Pageable pageable = PageRequest.of(0, 150);
            Long carpetaId = Long.parseLong(filters.get(CONST_CARPETA_ID));
            List<HistoryTaskRestoSearch> histories = historyTaskRestoSearchRepositorio.findByCarpetaId(carpetaId, pageable);

            return mapperList(histories);

        } catch (Exception e) {
            throw new CustomException(ErrorCode.INTERNAL_ERROR);
        }
    }

    @Override
    public String findActividad(String qparams) throws CustomException {
        try {
            var json = UtilJson.getValues(qparams);
            Map<String, String> filters = DividerJsonParameters.getFilters(json, CONST_FILTERS);
            Specification<HistoryTaskResto> specification = Specification.where(
                    specificationBuilder.withParameterOfTheEntityItself(filters.get(CONST_CARPETA_ID), CONST_CARPETA_ID));
            var activity = historyTaskRestoRepositorio.findAll(specification, Sort.by(Direction.DESC, "id"));
            return activity.get(0).getId();
        } catch (Exception e) {
            throw new CustomException(ErrorCode.INTERNAL_ERROR);
        }
    }

    @Override
    public List<TaskDTO> findListActividades(String qparams) throws CustomException {
        logger.info("Entrando a findListActividades");
        try {
            List<TaskDTO> taskDTOList = new ArrayList<>();
            var json = UtilJson.getValues(qparams);
            Map<String, String> filters = DividerJsonParameters.getFilters(json, CONST_FILTERS);

            Specification<HistoryTaskResto> specification = (root, query, builder) -> {
                List<Predicate> predicates = new ArrayList<>();
                predicates.add(builder.and(builder.equal(root.get(CONST_CARPETA_ID), filters.get(CONST_CARPETA_ID)), builder.isNotNull(root.get("runningTask").get("id"))));

                return builder.and(predicates.toArray(new Predicate[0]));
            };
            var activity = historyTaskRestoRepositorio.findAll(specification, Sort.by(Direction.DESC, "id"));
            for (HistoryTaskResto htr : activity) {
                if (Objects.nonNull(htr.getRunningTask())) {
                    var taskDTO = new TaskDTO();
                    taskDTO.setIdTask(htr.getRunningTask().getId());
                    taskDTO.setTaskDefKey(htr.getRunningTask().getTaskDefKey());
                    taskDTO.setActNom(htr.getRunningTask().getName());
                    taskDTO.setIdExecution(htr.getRunningTask().getRunningExecution().getId());
                    taskDTO.setProcDefKey(htr.getRunningTask().getProcessDefinition().getKey());
                    taskDTO.setCarpetaId(htr.getCarpeta().getId());
                    if (Objects.nonNull(htr.getRunningTask().getUsuario())) {
                        String name = (htr.getRunningTask().getUsuario().getPrimerNombre() != null ?
                                htr.getRunningTask().getUsuario().getPrimerNombre().concat(" ") : "").concat(htr.getRunningTask().getUsuario().getApellidoPaterno() != null ?
                                htr.getRunningTask().getUsuario().getApellidoPaterno() : "");
                        taskDTO.setResponsableNombre(name);
                    }
                    var dateFormat = new SimpleDateFormat(DATE_FORMAT);
                    String fechaInicio = null;
                    if (Objects.nonNull(htr.getRunningTask().getCreateTime())) {
                        fechaInicio = dateFormat.format(htr.getRunningTask().getCreateTime());
                    }
                    Optional<RunningTaskResto> rtr = runningTaskRestoRepositorio.findById(htr.getRunningTask().getId());
                    if(rtr.isPresent()) {
                    	taskDTO.setRol(new RolDTO(rtr.get().getRol().getId(), rtr.get().getRol().getDescripcion()));
                    }
                    taskDTO.setFechaCreacion(fechaInicio);
                    taskDTOList.add(taskDTO);
                }
            }
            return taskDTOList;
        } catch (Exception e) {
            throw new CustomException(ErrorCode.INTERNAL_ERROR);
        }
    }

    @Override
    public PerfilamientoReasignar findByInstitucion(String qparams) throws CustomException {
        try {
            var json = UtilJson.getValues(qparams);
            Map<String, String> filters = DividerJsonParameters.getFilters(json, CONST_FILTERS);
            Specification<PerfilamientoReasignar> specification = Specification.where(
            		specificationBuilderReasignar.withParameterOfAnotherEntity(filters.get(CONST_INSTITUCION_ID), "institucion", "id"));
            var perfilamientoReasignars = perfilamientoReasignarRepositorio.findAll(specification, Sort.by(Direction.DESC, "id"));
            if (!perfilamientoReasignars.isEmpty()) {
                return perfilamientoReasignars.get(0);
            } else {
                return null;
            }
        } catch (Exception e) {
            throw new CustomException(ErrorCode.INTERNAL_ERROR);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public ListaTrabajoFiltros findListaTrabajoFilters(String qParams) throws CustomException {
        try {
            var json = UtilJson.getValues(qParams);
            Map<String, String> filters = DividerJsonParameters.getFilters(json, CONST_FILTERS);
            long startTime = System.nanoTime();
            String username = filters.get(CONST_USERNAME);
            UsuarioBd usuario = usuarioRepositorio.findByNombreUsuario(username);
            logger.info("usuario ID: {}", usuario.getId());

            Long[] usuariosIds = setUsersIdsIfUserSupervisor(usuario);

            var sb = new StringBuilder(Constants.QUERY_LIST_WORK);
            var sbClausules = new ArrayList<String>();
            if (Objects.isNull(usuario.getAdminSistema()) || Boolean.FALSE.equals(usuario.getAdminSistema())) {
                sbClausules.add(CONST_COLUMN_USERS);
            }
            sb.append(extractQuery(sbClausules));
            
            var query = entityManager.createNativeQuery(sb.toString());

            if (Objects.isNull(usuario.getAdminSistema()) || Boolean.FALSE.equals(usuario.getAdminSistema())) {
            	query.setParameter(CONST_USER_ID, Arrays.asList(usuariosIds));
            }
            
            List<Object[]> iListaTrabajos = query.getResultList();
            HashSet<SucursalBanco> sucursales = new HashSet<>();
            HashSet<RegionDTO> regiones = new HashSet<>();
            HashSet<SegmentoDTO> segmentos = new HashSet<>();
            HashSet<DestinoCreditoDTO> destinos = new HashSet<>();
            HashSet<FilterFlujoCanal> flujos = new HashSet<>();
            HashSet<String> actividades = new HashSet<>();

            List<Object[]> iListaTrabajosFiltrada = new ArrayList<>();
            String filtroEstandar = filters.get(CONST_ESTANDAR_FILTER);

            for (Object[] listaTrabajo : iListaTrabajos) {
                if (Objects.isNull(filtroEstandar)) {
                    iListaTrabajosFiltrada.add(listaTrabajo);
                } else {
                    String dentroEstandar = calcularEstandar(listaTrabajo[27], listaTrabajo[9], listaTrabajo[10]);
                    if (dentroEstandar.equalsIgnoreCase(filtroEstandar)) {
                        iListaTrabajosFiltrada.add(listaTrabajo);
                    }
                }
            }

            for (Object[] listaTrabajo : iListaTrabajosFiltrada) {
                
            	actividades.add((String) listaTrabajo[8]);

            	if(Objects.nonNull(listaTrabajo[21]) && Objects.nonNull(listaTrabajo[22])){
            		flujos.add(setFilterFlujoCanalHashSet(listaTrabajo[21], listaTrabajo[22], username));
            	}
            	if(Objects.nonNull(listaTrabajo[23])){
            		sucursales.add(setSucursalesHashSet(listaTrabajo[23]));
            	}
            	if(Objects.nonNull(listaTrabajo[24])){
            		regiones.add(setRegionesHashSet(listaTrabajo[24]));
            	}
            	if(Objects.nonNull(listaTrabajo[25])){
            		segmentos.add(setSegmentosHashSet(listaTrabajo[25]));
            	}
                if(Objects.nonNull(listaTrabajo[26])){
                	destinos.add(setDestinosCreditosHashSet(listaTrabajo[26]));
                }

            }

            //FlujosCanales:
            List<FilterFlujoCanal> listflujosCanales = setFilterFlujoCanal(flujos);
            //Sucursales:
            List<SucursalBanco> listSucursalBancos = setSucursales(sucursales);
            //Regiones:
            List<RegionDTO> listRegiones = setRegiones(regiones);
            //Segmentos:
            List<SegmentoDTO> listSegmentos = setSegmentos(segmentos);
            //Destinos del Credito:
            List<DestinoCreditoDTO> listDestinos = setDestinosCredito(destinos);
            var listaTrabajoFiltros = new ListaTrabajoFiltros();
            listaTrabajoFiltros.setActividades(new ArrayList<>(actividades));
            Collections.sort(listflujosCanales, (o1, o2) -> o1.getFlujoCanal().compareTo(o2.getFlujoCanal()));
            listaTrabajoFiltros.setCanales(listflujosCanales);
            Collections.sort(listDestinos, (o1, o2) -> o1.getDescripcion().compareTo(o2.getDescripcion()));
            listaTrabajoFiltros.setDestinos(listDestinos);
            Collections.sort(listRegiones, (o1, o2) -> o1.getNombre().compareTo(o2.getNombre()));
            listaTrabajoFiltros.setRegiones(listRegiones);
            Collections.sort(listSegmentos, (o1, o2) -> o1.getNombre().compareTo(o2.getNombre()));
            listaTrabajoFiltros.setSegmentos(listSegmentos);
            Collections.sort(listSucursalBancos, (o1, o2) -> o1.getDescripcion().compareTo(o2.getDescripcion()));
            listaTrabajoFiltros.setSucursales(listSucursalBancos);

            long endTime = System.nanoTime();
            logger.info("segundos: {}", (endTime - startTime));
            return listaTrabajoFiltros;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new CustomException(ErrorCode.INTERNAL_ERROR);
        }
    }

    @Override
    public void findAsistidos(UsuarioBd usuario, List<Usuario> responsables) {
        var asistentesList = asistenteRepositorio.findAllByAsistente(usuario.getId());
        if (asistentesList.size() > 0){
            for ( Asistentes asistente : asistentesList) {
                var asistido = usuarioRepositorio.findById(asistente.getUsuario());
                asistido.ifPresent(usuarioBd -> responsables.add(mapper(usuarioBd)));
            }
        }
    }

    @Override
    public List<Usuario> findResponsables(String qparams) throws CustomException {
        try {
            var json = UtilJson.getValues(qparams);
            Map<String, String> filters = DividerJsonParameters.getFilters(json, CONST_FILTERS);
            var username = filters.get(CONST_USERNAME);
            var usuario = usuarioRepositorio.findByNombreUsuario(username);
            List<Usuario> responsables = new ArrayList<>();
            responsables.add(mapper(usuario));
            findAsistidos(usuario, responsables);
            if (Boolean.TRUE.equals(usuario.getIsSupervisor())) {
                responsables.addAll(mapperUsuarios(usuarioRepositorio.findBySupervisorId(usuario.getId())));
            }
            return responsables;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new CustomException(ErrorCode.INTERNAL_ERROR);
        }
    }

    @Override
    public List<Region> findRegiones(String qparams) throws CustomException {

        try {
            List<Region> regiones = new ArrayList<>();
            var json = UtilJson.getValues(qparams);
            Map<String, String> filters = DividerJsonParameters.getFilters(json, CONST_FILTERS);
            Object value = filters.get(CONST_IDS);
            if (value != null) {
                @SuppressWarnings("unchecked")
                ArrayList<Integer> ids = (ArrayList<Integer>) value;
                for (Integer id : ids) {
                    Optional<Region> opRegion = regionRepositorio.findById(Long.valueOf(id.longValue()));
                    if (opRegion.isPresent()) {
                        regiones.add(opRegion.get());
                    }
                }
            }
            return regiones;
        } catch (Exception e) {
            throw new CustomException(ErrorCode.INTERNAL_ERROR);
        }
    }

    @Override
    public TareasPendientesResponse findTareasPendientes(String qparams) throws CustomException {

        var tareasPendientesResponse = new TareasPendientesResponse();
        tareasPendientesResponse.setCountDentroEstandar(0);
        tareasPendientesResponse.setCountDentroEstandarLimite(0);
        tareasPendientesResponse.setCountFueraEstandar(0);
        try {
            var json = UtilJson.getValues(qparams);
            Map<String, String> filters = DividerJsonParameters.getFilters(json, CONST_FILTERS);
            var conteoInicial = Boolean.valueOf(filters.get("conteoInicial"));
            Long[] usuariosIds;
            List<Usuario> usuarios = new ArrayList<>();
            if (Boolean.TRUE.equals(conteoInicial)) {
                usuarios = findResponsables(qparams);
                usuariosIds = new Long[usuarios.size()];
                var indice = 0;
                for (Usuario usuario : usuarios) {
                    usuariosIds[indice++] = usuario.getId();
                }
            } else {
                var usuario = usuarioRepositorio.findByNombreUsuario(filters.get(CONST_USERNAME));
                var indice = 0;
                usuariosIds = new Long[usuarios.size()+1];
                usuariosIds[indice++] = usuario.getId();
                for (Usuario user : usuarios) {
                    usuariosIds[indice++] = user.getId();
                }
            }

            List<RunningTaskDTO> tasks = consultaListaTrabajo(Arrays.asList(usuariosIds));
            setInicioConteoTareas(tareasPendientesResponse, tasks, filters.get(CONST_ESTANDAR_FILTER));

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new CustomException(ErrorCode.INTERNAL_ERROR);
        }
        return tareasPendientesResponse;
    }

    //#region Metodos Auxiliares

    private List<DestinoCreditoDTO> setDestinosCredito(HashSet<DestinoCreditoDTO> destinos) {
        List<DestinoCreditoDTO> listDestinos = new ArrayList<>(destinos);
        for (DestinoCreditoDTO destinoCreditoDTO : listDestinos) {
            Optional<DestinoCredito> opDestino = destinoCreditoRepositorio.findById(destinoCreditoDTO.getId());
            opDestino.ifPresent(destinoCredito -> destinoCreditoDTO.setDescripcion(destinoCredito.getDescripcion()));
        }
        return listDestinos;
    }

    private List<SegmentoDTO> setSegmentos(HashSet<SegmentoDTO> segmentos) {
        List<SegmentoDTO> listSegmentos = new ArrayList<>(segmentos);
        for (SegmentoDTO segmentoDTO : listSegmentos) {
            Optional<Segmento> opSegmento = segmentoRepositorio.findById(segmentoDTO.getId());
            opSegmento.ifPresent(segmento -> segmentoDTO.setNombre(segmento.getNombre()));
        }
        return listSegmentos;
    }

    private List<RegionDTO> setRegiones(HashSet<RegionDTO> regiones) {
        List<RegionDTO> listRegiones = new ArrayList<>(regiones);
        for (RegionDTO region : listRegiones) {
            Optional<Region> opRegion = regionRepositorio.findById(region.getId());
            opRegion.ifPresent(value -> region.setNombre(value.getNombre()));
        }
        return listRegiones;
    }

    private List<SucursalBanco> setSucursales(HashSet<SucursalBanco> sucursales) {
        List<SucursalBanco> listSucursalBancos = new ArrayList<>(sucursales);
        for (SucursalBanco sucursalBanco : listSucursalBancos) {
            Optional<Sucursal> opSucursalBanco = sucursalBancoRepositorio.findById(sucursalBanco.getId());
            opSucursalBanco.ifPresent(sucursal -> sucursalBanco.setDescripcion(sucursal.getDescripcion()));
        }
        return listSucursalBancos;
    }

    private List<FilterFlujoCanal> setFilterFlujoCanal(HashSet<FilterFlujoCanal> flujos) {
        List<FilterFlujoCanal> listflujosCanales = new ArrayList<>(flujos);
        for (FilterFlujoCanal filterFlujoCanal : listflujosCanales) {
            Optional<Canal> opCanal = canalRepositorio.findById(filterFlujoCanal.getIdCanal());
            Optional<Flujo> opFlujo = flujoRepositorio.findById(filterFlujoCanal.getIdFlujo());
            if (opCanal.isPresent() && opFlujo.isPresent()) {
                filterFlujoCanal.setFlujoCanal(opCanal.get().getDescripcion() + " (" + opFlujo.get().getDescripcion() + ")");
            }
        }
        return listflujosCanales;
    }

    private DestinoCreditoDTO setDestinosCreditosHashSet(Object item) {
       var destinoCreditoDTO = new DestinoCreditoDTO();
       destinoCreditoDTO.setId(((BigInteger) item).longValue());
       return destinoCreditoDTO;
    }

    private SegmentoDTO setSegmentosHashSet(Object item) {
        var segmentoDTO = new SegmentoDTO();
        segmentoDTO.setId(((BigInteger) item).longValue());
        return segmentoDTO;
    }

    private RegionDTO setRegionesHashSet(Object item) {
        
    	var regionDTO = new RegionDTO();
        regionDTO.setId(((BigInteger) item).longValue());
        return regionDTO;
    }

    private FilterFlujoCanal setFilterFlujoCanalHashSet(Object idCanal, Object idFlujo, String username) {
        
    	var flujo = new FilterFlujoCanal();
        flujo.setIdCanal(((BigInteger) idCanal).longValue());
        flujo.setIdFlujo(((BigInteger) idFlujo).longValue());
        flujo.setFlujoCanal(username);
        return flujo;
    }

    private SucursalBanco setSucursalesHashSet(Object item) {
        
    	var sucursal = new SucursalBanco();
        sucursal.setId(((BigInteger) item).longValue());
        return sucursal;
    }

    private PaginadoResponse getResponseListaTrabajo(List<ListaTrabajo> listaTrabajos,
                                                     String filtroEstandar, int totalPages,
                                                     long totalElement, int totalClientes) {
        List<ListaTrabajo> listaDentroStandar = new ArrayList<>();
        List<ListaTrabajo> listaDentroStandarLimite = new ArrayList<>();
        List<ListaTrabajo> listaFueraStandar = new ArrayList<>();

        for (ListaTrabajo listaTrabajo : listaTrabajos) {
            if (listaTrabajo.getDentroEstandar().toString().equalsIgnoreCase(DENTRO_ESTANDAR)) {
                listaDentroStandar.add(listaTrabajo);
            }
            if (listaTrabajo.getDentroEstandar().toString().equalsIgnoreCase(DENTRO_ESTANDAR_LIMITE)) {
                listaDentroStandarLimite.add(listaTrabajo);
            } else {
                listaFueraStandar.add(listaTrabajo);
            }
        }
        if (DENTRO_ESTANDAR.equalsIgnoreCase(filtroEstandar)) {
            return new PaginadoResponse(totalPages, listaDentroStandar, totalElement, totalClientes);
        } else if (DENTRO_ESTANDAR_LIMITE.equalsIgnoreCase(filtroEstandar)) {
            return new PaginadoResponse(totalPages, listaDentroStandarLimite, totalElement, totalClientes);
        } else if (FUERA_ESTANDAR.equalsIgnoreCase(filtroEstandar)) {
            return new PaginadoResponse(totalPages, listaFueraStandar, totalElement, totalClientes);
        } else {
            return new PaginadoResponse(totalPages, listaTrabajos, totalElement, totalClientes);
        }
    }

    private void setUserIdToQuery(Query query, UsuarioBd usuario, Boolean conteoInicial, Long[] usuariosIds) {
        if ((Objects.isNull(usuario.getAdminSistema()) || !usuario.getAdminSistema())
                || (usuario.getAdminSistema() && Boolean.FALSE.equals(conteoInicial))) {
            query.setParameter(CONST_USER_ID, Arrays.asList(usuariosIds));
        }
    }

    private Long[] setUsersId(
            UsuarioBd usuario,
            Boolean conteoInicial) {
        Long[] usuariosIds;
        if (Boolean.FALSE.equals(conteoInicial)) {
            usuariosIds = new Long[1];
            usuariosIds[0] = usuario.getId();
        } else {
            usuariosIds = setUsersIdsIfUserSupervisor(usuario);
        }
        return usuariosIds;
    }

    private Long[] setUsersIdsIfUserSupervisor(
            UsuarioBd usuario) {
        Long[] usuariosIds;
        List<Long> usuariosId = new ArrayList<>();
        usuariosId.add(usuario.getId());
        List<Usuario> responsables = new ArrayList<>();
        findAsistidos(usuario, responsables);
        for(Usuario user : responsables){
            usuariosId.add(user.getId());
        }
        if (Boolean.TRUE.equals(usuario.getIsSupervisor())) {
            List<UsuarioBd> supervisados = usuarioRepositorio.findBySupervisorId(usuario.getId());
            for (UsuarioBd usuarioBd : supervisados) {
                usuariosId.add(usuarioBd.getId());
            }
        }
        usuariosIds = new Long[usuariosId.size()];
        var indice = 0;
        for (Long id : usuariosId) {
            usuariosIds[indice++] = id;
        }
        return usuariosIds;
    }

    private void setFilters(Map<String, String> filters,
                            StringBuilder sb,
                            StringBuilder sbCount,
                            StringBuilder sbCountDistintRut,
                            UsuarioBd usuario,
                            Boolean conteoInicial) {
        var sbClausules = new ArrayList<String>();
        var sbCountClausules = new ArrayList<String>();
        var sbCountDistintRutClausules = new ArrayList<String>();

        //#region Filtros

        var numberFolder = Optional.ofNullable(filters.get("numCarpeta")).orElse("");
        if (!numberFolder.isEmpty()) {
            final var concat = CONST_COLUMN_CARPETA_ID + Integer.parseInt(numberFolder);
            sbClausules.add(concat);
            sbCountClausules.add(concat);
            sbCountDistintRutClausules.add(concat);
        }
        String rut = Optional.ofNullable(filters.get("rut")).orElse("");
        if (!rut.isEmpty()) {
            final var concat = CONST_COLUMN_RUT.concat("'").concat(rut).concat("'");
            sbClausules.add(concat);
            sbCountClausules.add(concat);
            sbCountDistintRutClausules.add(concat);
        }
        var segmentoAtencion = Optional.ofNullable(filters.get("segmento")).orElse("");
        if (!segmentoAtencion.isEmpty()) {
            final var concat = CONST_COLUMN_SEGMENTO_ATENCION_ID + (Long.valueOf(segmentoAtencion));
            sbClausules.add(concat);
            sbCountClausules.add(concat);
            sbCountDistintRutClausules.add(concat);
        }
        var destiny = Optional.ofNullable(filters.get("destino")).orElse("");
        if (!destiny.isEmpty()) {
            final var concat = CONST_COLUMN_DESTINO_ID + Long.valueOf(destiny);
            sbClausules.add(concat);
            sbCountClausules.add(concat);
            sbCountDistintRutClausules.add(concat);
        }
        var canal = Optional.ofNullable(filters.get("canal")).orElse("");
        if (!canal.isEmpty()) {
            final var concat = CONST_COLUMN_CANAL_ID + (Long.valueOf(canal));
            sbClausules.add(concat);
            sbCountClausules.add(concat);
            sbCountDistintRutClausules.add(concat);
        }
        var flujo = Optional.ofNullable(filters.get("flujo")).orElse("");
        if (!flujo.isEmpty()) {
            final var concat = CONST_COLUMN_FLUJO_ID + (Long.valueOf(flujo));
            sbClausules.add(concat);
            sbCountClausules.add(concat);
            sbCountDistintRutClausules.add(concat);
        }
        var region = Optional.ofNullable(filters.get("region")).orElse("");
        if (!region.isEmpty()) {
            final var concat = CONST_COLUMN_REGION_ID + (Long.valueOf(region));
            sbClausules.add(concat);
            sbCountClausules.add(concat);
            sbCountDistintRutClausules.add(concat);
        }
        var name = Optional.ofNullable(filters.get("nombre")).orElse("");
        if (!name.isEmpty()) {
            final var concat = CONST_COLUMN_NOMBRE.concat("'%").concat(name).concat("%'");
            sbClausules.add(concat);
            sbCountClausules.add(concat);
            sbCountDistintRutClausules.add(concat);
        }
        var apellidos = Optional.ofNullable(filters.get("apellidoP")).orElse("");
        if (!apellidos.isEmpty()) {
            final var concat = CONST_COLUMN_APELLIDO_PRIMERO.concat("'%").concat(apellidos).concat("%'");
            sbClausules.add(concat);
            sbCountClausules.add(concat);
            sbCountDistintRutClausules.add(concat);
        }
        var apellidosM = Optional.ofNullable(filters.get("apellidoM")).orElse("");
        if (!apellidosM.isEmpty()) {
            final var concat = CONST_COLUMN_APELLIDO_SEGUNDO.concat("'%").concat(apellidosM).concat("%'");
            sbClausules.add(concat);
            sbCountClausules.add(concat);
            sbCountDistintRutClausules.add(concat);
        }
        var actividad = Optional.ofNullable(filters.get("actividad")).orElse("");
        if (!actividad.isEmpty()) {
            final var concat = CONST_COLUMN_ACTIVIDAD.concat("'").concat(actividad).concat("'");
            sbClausules.add(concat);
            sbCountClausules.add(concat);
            sbCountDistintRutClausules.add(concat);
        }
        var estado = Optional.ofNullable(filters.get("estado")).orElse("");
        if (!estado.isEmpty()) {
            final var concat = CONST_COLUMN_DATE_RUNNING.concat("'").concat(estado).concat("'");
            sbClausules.add(concat);
            sbCountClausules.add(concat);
            sbCountDistintRutClausules.add(concat);
        }
        if ((usuario.getAdminSistema() == null || !usuario.getAdminSistema())
                || (usuario.getAdminSistema() && Boolean.FALSE.equals(conteoInicial))) {
            sbClausules.add(CONST_COLUMN_USERS);
            sbCountClausules.add(CONST_COLUMN_USERS);
            sbCountDistintRutClausules.add(CONST_COLUMN_USERS);
        }

        sb.append(extractQuery(sbClausules));
        sbCount.append(extractQuery(sbCountClausules));
        sbCountDistintRut.append(extractQuery(sbCountDistintRutClausules));

        sb.append(" ORDER BY PRIORIDAD_col_2_0_ ASC, id_carpeta_col_0_0_ DESC");

        //#endregion
    }

    private List<HistoricoResponse> mapperList(List<HistoryTaskRestoSearch> historicos) {

        List<HistoricoResponse> response = new ArrayList<>();

        for (HistoryTaskRestoSearch history : historicos) {
            response.add(mapper(history));
        }
        Collections.sort(response, (o1, o2) -> o2.getE().compareTo(o1.getE()));
        return response;
    }

    private HistoricoResponse mapper(HistoryTaskRestoSearch historyTaskResto) {

        HistoryTaskSearch history = historyTaskResto.getHistoryTask();
        var usuario = history.getUsuario() == null ? new UsuarioBd() : history.getUsuario();
        var primerNomber = StringUtils.hasText(usuario.getPrimerNombre()) ? usuario.getPrimerNombre() : "";
        var apellidoPaterno = StringUtils.hasText(usuario.getApellidoPaterno()) ? usuario.getApellidoPaterno() : "";
        
        return new HistoricoResponse(
                history.getHistoryTaskResto().getId(),
                history.getName(),
                history.getTaskDefKey(),
                history.getDeleteReason() != null ? history.getDeleteReason() : "A",
                history.getStartTime(),
                history.getDueDate(),
                history.getEndTime(),
                primerNomber.concat(" ").concat(apellidoPaterno),
                history.getHistoryTaskResto().getTEstandarD(),
                history.getHistoryTaskResto().getTEstandarH(),
                history.getHistoryTaskResto().getTRealD(),
                history.getHistoryTaskResto().getTRealH(),
                history.getHistoryTaskResto().getCarpeta().getCarpetaPadre() != null ? "principal" : "auxiliar",
                calcularEstandar(history.getStartTime(), history.getStartTime(), history.getDueDate()),
                history.getHistoryTaskResto().getCarpeta().getId(),
                history.getHistoryTaskResto().getCarpetaId() != null ? "principal" : "auxiliar",
                history.getName(),
                history.getHistoryTaskResto().getCarpeta().getTipoOperacionId());
    }

    private String extractQuery(List<String> clausules) {

        var query = new StringBuilder();
        if (!clausules.isEmpty()) {
            for (var i = 0; i < clausules.size(); i++) {
                if (i == 0) {
                    query.append(" WHERE ").append(clausules.get(i));
                } else {
                    query.append(" AND ").append(clausules.get(i));
                }

            }
        }

        return query.toString();
    }

    private CarpetaResponse mapper(Carpeta carpeta) {

        var carpetaResponse = new CarpetaResponse();
        carpetaResponse.setId(carpeta.getId());
        var dateFormat = new SimpleDateFormat(DATE_FORMAT);
        carpetaResponse.setFechaFormarteada(dateFormat.format(carpeta.getFechaIngreso()));
        carpetaResponse.setFechaIngreso(carpeta.getFechaIngreso());
        if (carpeta.getFlujo() != null) {
            var flujo = new com.cl.wh2listadetrabajo.dtos.Flujo();
            flujo.setId(carpeta.getFlujo().getId());
            flujo.setDescripcion(carpeta.getFlujo().getDescripcion());
            flujo.setProcDefId(carpeta.getFlujo().getProcDefId());
            carpetaResponse.setFlujo(flujo);
        }
        if (carpeta.getCanal() != null) {
            var canal = new com.cl.wh2listadetrabajo.dtos.Canal();
            canal.setId(carpeta.getCanal().getId());
            canal.setDescripcion(carpeta.getCanal().getDescripcion());
            carpetaResponse.setCanal(canal);
        }
        if (carpeta.getSucursalBanco() != null) {
            var sucursal = new SucursalBanco();
            sucursal.setId(carpeta.getSucursalBanco().getId());
            sucursal.setDescripcion(carpeta.getSucursalBanco().getDescripcion());
            carpetaResponse.setSucursalBanco(sucursal);
        }

        if (carpeta.getEstadoCarpeta() != null) {
            var estadoCarpeta = new EstadoCarpeta();
            estadoCarpeta.setId(carpeta.getEstadoCarpeta().getId());
            estadoCarpeta.setNombre(carpeta.getEstadoCarpeta().getNombre());
            estadoCarpeta.setActivo(carpeta.getEstadoCarpeta().getActivo());
            carpetaResponse.setEstado(estadoCarpeta);
        }
        return carpetaResponse;
    }

    private Usuario mapper(UsuarioBd usuarioBd) {

        var usuario = new Usuario();
        usuario.setId(usuarioBd.getId());
        usuario.setNombreUsuario(usuarioBd.getNombreUsuario());
        usuario.setNombreCompleto(getNombreCompletoUsuario(usuarioBd));
        return usuario;

    }

    private String getNombreCompletoUsuario(UsuarioBd usuario) {

        String primerN = usuario.getPrimerNombre() == null ? "" : usuario.getPrimerNombre().concat(" ");
        String apellidoP = usuario.getApellidoPaterno() == null ? "" : usuario.getApellidoPaterno().concat(" ");
        String apellidoM = usuario.getApellidoMaterno() == null ? "" : usuario.getApellidoMaterno();
        return primerN.concat(apellidoP).concat(apellidoM);
    }

    private List<Usuario> mapperUsuarios(List<UsuarioBd> usuariosBd) {

        List<Usuario> usuarios = new ArrayList<>();
        for (UsuarioBd usuario : usuariosBd) {
            usuarios.add(mapper(usuario));
        }
        return usuarios;
    }

    private void setInicioConteoTareas(TareasPendientesResponse tareasPendientesResponse, List<RunningTaskDTO> tasks,
                                       String filtroEstandar) {

        HashSet<Long> flujos = new HashSet<>();
        List<RunningTaskDTO> tasksDentroStandar = new ArrayList<>();
        List<RunningTaskDTO> tasksDentroStandarLimite = new ArrayList<>();
        List<RunningTaskDTO> tasksFueraStandar = new ArrayList<>();
        var dentroEstandar = 0;
        var dentroEstandarLimite = 0;
        var fueraEstandar = 0;
        tareasPendientesResponse.setCountTotal(tasks.size());

        for (RunningTaskDTO task : tasks) {
            flujos.add(task.getProcessDefKey());
            if (task.getDentroEstandar().equalsIgnoreCase(DENTRO_ESTANDAR)) {
                tareasPendientesResponse.setCountDentroEstandar(++dentroEstandar);
                tasksDentroStandar.add(task);
            }else if (task.getDentroEstandar().equalsIgnoreCase(DENTRO_ESTANDAR_LIMITE)) {
                tareasPendientesResponse.setCountDentroEstandarLimite(++dentroEstandarLimite);
                tasksDentroStandarLimite.add(task);
            } else {
                tareasPendientesResponse.setCountFueraEstandar(++fueraEstandar);
                tasksFueraStandar.add(task);
            }
        }

        if (DENTRO_ESTANDAR.equalsIgnoreCase(filtroEstandar)) {
            setConteoFiltrado(flujos, tareasPendientesResponse, tasksDentroStandar);
        } else if (DENTRO_ESTANDAR_LIMITE.equalsIgnoreCase(filtroEstandar)) {
            setConteoFiltrado(flujos, tareasPendientesResponse, tasksDentroStandarLimite);
        } else if (FUERA_ESTANDAR.equalsIgnoreCase(filtroEstandar)) {
            setConteoFiltrado(flujos, tareasPendientesResponse, tasksFueraStandar);
        } else {
            setConteoFiltrado(flujos, tareasPendientesResponse, tasks);
        }
    }

    private List<TareasPendientesDTO> getTareasPendientes(String procDefId,
                                                          Map<String, String> valores,
                                                          HashSet<String> tareas,
                                                          FlujosPendientesDTO flujoPendiente,
                                                          List<RunningTaskDTO> tasks) {
        List<TareasPendientesDTO> tareasPendientes = new ArrayList<>();
        for (String tarea : tareas) {
            var tareasPendientesDTO = new TareasPendientesDTO();
            tareasPendientesDTO.setTarea(tarea);
            tareasPendientesDTO.setTaskDefKey(valores.get(tarea));
            tareasPendientesDTO.setAvanceMasivo(this.habilitadoAvanceMasivo(procDefId, valores.get(tarea)));
            tareasPendientesDTO.setFlujoId(flujoPendiente.getFlujoId());
            tareasPendientesDTO.setFlujo(flujoPendiente.getFlujo());
            var cantidad = 0;
            for (RunningTaskDTO task : tasks) {
                if (task.getTaskDef().equalsIgnoreCase(tarea)) {
                    tareasPendientesDTO.setCantidad(++cantidad);
                }
            }
            if (tareasPendientesDTO.getCantidad() > 0) {
                tareasPendientes.add(tareasPendientesDTO);
            }
        }
        return tareasPendientes;
    }

    private void setConteoFiltrado(HashSet<Long> flujos,
                                   TareasPendientesResponse tareasPendientesResponse,
                                   List<RunningTaskDTO> tasks) {

        List<FlujosPendientesDTO> flujosPendientes = new ArrayList<>();
        for (long flujo : flujos) {
            var flujoPendiente = new FlujosPendientesDTO();
            flujoPendiente.setFlujoId(flujo);
            var modelFlujo = flujoRepositorio.findById(flujo).orElse(new Flujo());
            flujoPendiente.setFlujo(modelFlujo.getDescripcion());

            HashSet<String> tareas = new HashSet<>();
            Map<String, String> valores = new HashMap<>();
            for (RunningTaskDTO task : tasks) {
                if (task.getProcessDefKey() == flujo) {
                    tareas.add(task.getTaskDef());
                    valores.put(task.getTaskDef(), task.getTaskDefKey());
                }
            }

            var procDefId = modelFlujo.getProcDefId();
            List<TareasPendientesDTO> tareasPendientes = getTareasPendientes(procDefId, valores, tareas, flujoPendiente, tasks);

            if (!tareasPendientes.isEmpty()) {
                flujoPendiente.setTareas(tareasPendientes);
                flujosPendientes.add(flujoPendiente);
            }
        }

        tareasPendientesResponse.setFlujos(flujosPendientes);

    }

    private Boolean habilitadoAvanceMasivo(String procDefKey, String taskKey) {

    	return customTaskDefinitionRepositorio.existsByProcDefKeyAndTaskKey(procDefKey,taskKey);
    }

    @SuppressWarnings("unchecked")
    private List<RunningTaskDTO> consultaListaTrabajo(List<Long> usuariosIds) throws CustomException {
        try {

            var sb = new StringBuilder(Constants.QUERY_LIST_WORK);
            var sbClausules = new ArrayList<String>();
            sbClausules.add(createUserClausule(usuariosIds));
            sb.append(extractQuery(sbClausules));
            var query = entityManager.createNativeQuery(sb.toString());
            List<Object[]> iListaTrabajos = query.getResultList();
            List<RunningTaskDTO> tasks = new ArrayList<>();
            for (Object[] listaTrabajo : iListaTrabajos) {
                var runningTaskDTO = new RunningTaskDTO();
                runningTaskDTO.setTaskDefKey((String) listaTrabajo[18]);
                runningTaskDTO.setTaskDef((String) listaTrabajo[8]);
                runningTaskDTO.setProcessDefKey(((BigInteger) listaTrabajo[22]).longValue());
                runningTaskDTO.setDueDate((Date) listaTrabajo[10]);
                runningTaskDTO.setDentroEstandar(calcularEstandar(listaTrabajo[27], listaTrabajo[9], listaTrabajo[10]));
                tasks.add(runningTaskDTO);
            }
            return tasks;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new CustomException(ErrorCode.INTERNAL_ERROR);
        }
    }

    private String calcularEstandar(Object fechaTerminada, Object fechaInicio, Object fechaEsperada) {

        final var PORCENTAJE_DENTRO_ESTANDAR = 76;
        var ahora = new Date();
        String estandar = null;
        if (Objects.nonNull(fechaTerminada)) {
        	ahora = (Date) fechaTerminada;
        }
        
        Date inicio = (Date) fechaInicio;
        Date esperada = (Date) fechaEsperada;
        if(Objects.isNull(esperada)) {
        	estandar = DENTRO_ESTANDAR;
        }else {
	        long total = esperada.getTime() - inicio.getTime();
	        long transcurrido = ahora.getTime() - inicio.getTime();
	        if(total > 0) {
		        long porcentaje = (transcurrido * 100) / total;
		        if(porcentaje > 100) {
		        	estandar = FUERA_ESTANDAR;
		        }else if (porcentaje <= PORCENTAJE_DENTRO_ESTANDAR) {
		            estandar = DENTRO_ESTANDAR;
		        } else {
		            estandar = DENTRO_ESTANDAR_LIMITE;
		        }
	        }else {
	        	estandar = DENTRO_ESTANDAR;
	        }
        }
	        return estandar;
    }

    //#endregion
    
    @Override
	public Boolean validarTieneFormulario(String qparams) throws CustomException {
		
		Boolean tieneFormulario = Boolean.FALSE;
		try {
            var json = UtilJson.getValues(qparams);
            Map<String, String> filters = DividerJsonParameters.getFilters(json, CONST_FILTERS);
            String procDefId = filters.get("procDefId");
            String taskDefKey = filters.get("taskDefKey");
            String[] procDefKey = procDefId.split(":");
            Optional<com.cl.wh2listadetrabajo.model.CustomTaskDefinition> opCustomTaskDef = customTaskDefinitionRepositorio
            														.findByProcDefKeyAndTaskKey(procDefKey[0], taskDefKey);
            if(opCustomTaskDef.isPresent() && Objects.nonNull(opCustomTaskDef.get().getFormKey())){
            		tieneFormulario = Boolean.TRUE;
            }
            
		} catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new CustomException(ErrorCode.INTERNAL_ERROR);
        }
		return tieneFormulario;
	}

	@Override
	public List<TareasAsignadasResponseDTO> cargarTareasAsignadas(String qparams) throws CustomException {
		
		try {
			var json = UtilJson.getValues(qparams);
	        Map<String, String> filters = DividerJsonParameters.getFilters(json, CONST_FILTERS);
	        Long[] usuariosIds;
	        UsuarioBd usuario = usuarioRepositorio.findByNombreUsuario(filters.get(CONST_USERNAME));
			if(Objects.nonNull(usuario)) {
				usuariosIds = new Long[1];
	            usuariosIds[0] = usuario.getId();
			}else{
				throw new CustomException(ErrorCode.INTERNAL_ERROR);
			}
			List<TareasAsignadasResponseDTO> tasks = consultaTareasPendientes(Arrays.asList(usuariosIds));
			Collections.sort(tasks, (o1, o2) -> o1.getNCarpeta().compareTo(o2.getNCarpeta()));
			return tasks;
		} catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new CustomException(ErrorCode.INTERNAL_ERROR);
        }
	}
	
	@SuppressWarnings("unchecked")
    private List<TareasAsignadasResponseDTO> consultaTareasPendientes(List<Long> usuariosIds) throws CustomException {
        try {

            var sb = new StringBuilder(Constants.QUERY_LIST_WORK);
            var sbClausules = new ArrayList<String>();
            sbClausules.add(createUserClausule(usuariosIds));
            sb.append(extractQuery(sbClausules));
            var query = entityManager.createNativeQuery(sb.toString());
            List<Object[]> iListaTrabajos = query.getResultList();
            List<TareasAsignadasResponseDTO> tasks = new ArrayList<>();
            for (Object[] listaTrabajo : iListaTrabajos) {
                var tareasAsignadas = new TareasAsignadasResponseDTO();
                tareasAsignadas.setNCarpeta(((BigInteger) listaTrabajo[0]).longValue());
                tareasAsignadas.setActividadActual((String) listaTrabajo[8]);
                tareasAsignadas.setRut((String) listaTrabajo[4]);
                tareasAsignadas.setFechaEsperada((Date) listaTrabajo[10]);
                tareasAsignadas.setFechaInicio((Date) listaTrabajo[9]);
                tareasAsignadas.setTaskId((String) listaTrabajo[15]);
                tareasAsignadas.setNombreCliente(listaTrabajo[5].toString().concat(" ").concat(listaTrabajo[6].toString()));
                tasks.add(tareasAsignadas);
            }
            return tasks;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new CustomException(ErrorCode.INTERNAL_ERROR);
        }
    }
	
	
	private String createUserClausule(List<Long> usuariosIds) {
		return usuariosIds.stream()
      	      .map(String::valueOf)
      	      .collect(Collectors.joining(",", "USUARIO_CREADOR_ID IN (", ")"));
	}

}
