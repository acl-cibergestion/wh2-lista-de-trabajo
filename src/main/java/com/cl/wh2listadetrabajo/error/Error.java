package com.cl.wh2listadetrabajo.error;

import lombok.Data;

@Data
public class Error {
    
	private int index;
    private ErrorCode code;
    private String detail;
}
