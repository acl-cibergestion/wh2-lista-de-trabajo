package com.cl.wh2listadetrabajo.error;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import java.util.Map;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_EMPTY;
import static com.fasterxml.jackson.databind.SerializationFeature.WRITE_DATES_AS_TIMESTAMPS;

public class UtilJson {
    private static final ObjectMapper MAPPER;

    private UtilJson() {
    }

    static {
        MAPPER = Jackson2ObjectMapperBuilder.json()
                .serializationInclusion(NON_EMPTY)
                .featuresToDisable(WRITE_DATES_AS_TIMESTAMPS)
                .modules(new JavaTimeModule())
                .build();
    }

    public static Map<String,String> getValues(String json) throws JsonProcessingException {
        return MAPPER.readValue(json, Map.class);
    }

     public static String getMapperToJsonFilter(String fieldName,String item) throws JsonProcessingException {
        var objectMapper = new ObjectMapper();
        var nodeRoot = MAPPER.createObjectNode();
        var filters = MAPPER.createObjectNode();
        filters.put(fieldName,item);
        nodeRoot.set("filters",filters);
        return objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(nodeRoot);
    }
}
