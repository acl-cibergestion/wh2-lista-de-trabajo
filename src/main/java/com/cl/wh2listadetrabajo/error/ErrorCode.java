package com.cl.wh2listadetrabajo.error;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public enum ErrorCode {

    //Internal Errors
    INTERNAL_ERROR(500, "INTERNAL_ERROR", "Internal Server Error", HttpStatus.INTERNAL_SERVER_ERROR),

    //ListaDeTrabajo Errors
    EXAMPLE(700, "EXAMPLE", "Example", HttpStatus.UNAUTHORIZED);

    private int index;
    private String code;
    private String msg;
    private HttpStatus status;

    ErrorCode(int index, String code, String msg, HttpStatus status) {
        this.index = index;
        this.code = code;
        this.msg = msg;
        this.status = status;
    }
}
