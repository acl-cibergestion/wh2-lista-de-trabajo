package com.cl.wh2listadetrabajo.repositorios;

import com.cl.wh2listadetrabajo.model.Asistentes;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AsistenteRepositorio extends JpaRepository<Asistentes,Long> {
    List<Asistentes> findAllByAsistente(Long id);
}
