package com.cl.wh2listadetrabajo.repositorios;

import com.cl.wh2listadetrabajo.model.HistoryTaskResto;
import com.cl.wh2listadetrabajo.model.RunningTask;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

public interface HistoryTaskRestoRepositorio extends JpaRepository<HistoryTaskResto, String>,
        JpaSpecificationExecutor<HistoryTaskResto> {

	@Query("select ht.runningTask from HistoryTaskResto ht join ht.runningTask rt join rt.usuario u where u.id in (?1)")
    List<RunningTask> findPendingTaskByUser(Long[] usuariosId);

	@Query("select ht.runningTask from HistoryTaskResto ht join ht.runningTask rt join rt.usuario u where u.id in (?1)")
    List<RunningTask> findPendingTaskByUser(List<Long> asList);

}
