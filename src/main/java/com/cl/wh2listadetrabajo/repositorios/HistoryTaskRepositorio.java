package com.cl.wh2listadetrabajo.repositorios;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import com.cl.wh2listadetrabajo.model.HistoryTask;


public interface HistoryTaskRepositorio extends JpaRepository<HistoryTask, String>, JpaSpecificationExecutor<HistoryTask> {
	
	@Query("select distinct ht.name from HistoryTask ht join ht.historyProcessInstance hp join hp.carpeta c join c.usuario u where u.nombreUsuario=?1")
    List<String> findDistinctHistoryTaskByUser(String nombreUsuario);

}
