package com.cl.wh2listadetrabajo.repositorios;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.cl.wh2listadetrabajo.model.FoChecklist;

public interface FoCheckListRepositorio extends JpaRepository<FoChecklist, Long>, 
													JpaSpecificationExecutor<FoChecklist> {

}
