package com.cl.wh2listadetrabajo.repositorios;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import com.cl.wh2listadetrabajo.model.Carpeta;
import com.cl.wh2listadetrabajo.model.Formalizacion;
import com.cl.wh2listadetrabajo.model.Segmento;

public interface FormalizacionRepositorio extends JpaRepository<Formalizacion, Long>, 
													JpaSpecificationExecutor<Formalizacion> {

	@Query("select distinct s from Formalizacion f join f.segmentoAtencion s join f.carpeta c join c.usuario u where u.nombreUsuario=?1")
    List<Segmento> findDistinctSegmentosByUsuario(String nombreUsuario);

	Formalizacion findByCarpeta(Carpeta carpeta);

}
