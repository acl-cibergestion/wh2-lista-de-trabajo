package com.cl.wh2listadetrabajo.repositorios;

import com.cl.wh2listadetrabajo.model.RunningTask;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface RunningTaskRepositorio extends JpaRepository<RunningTask, Long>,
													JpaSpecificationExecutor<RunningTask> {

}
