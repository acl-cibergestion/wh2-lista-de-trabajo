package com.cl.wh2listadetrabajo.repositorios;

import com.cl.wh2listadetrabajo.model.RunningTaskResto;

import org.springframework.data.jpa.repository.JpaRepository;

public interface RunningTaskRestoRepositorio extends JpaRepository<RunningTaskResto, String>{

}
