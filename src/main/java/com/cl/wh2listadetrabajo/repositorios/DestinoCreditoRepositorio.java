package com.cl.wh2listadetrabajo.repositorios;


import org.springframework.data.jpa.repository.JpaRepository;
import com.cl.wh2listadetrabajo.model.DestinoCredito;

public interface DestinoCreditoRepositorio extends JpaRepository<DestinoCredito, Long>{

}
