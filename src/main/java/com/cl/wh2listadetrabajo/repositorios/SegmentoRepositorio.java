package com.cl.wh2listadetrabajo.repositorios;


import org.springframework.data.jpa.repository.JpaRepository;
import com.cl.wh2listadetrabajo.model.Segmento;

public interface SegmentoRepositorio extends JpaRepository<Segmento, Long>{

}
