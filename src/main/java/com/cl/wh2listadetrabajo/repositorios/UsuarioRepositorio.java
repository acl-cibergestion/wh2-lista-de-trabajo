package com.cl.wh2listadetrabajo.repositorios;

import com.cl.wh2listadetrabajo.model.UsuarioBd;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface UsuarioRepositorio extends JpaRepository<UsuarioBd, Long>, JpaSpecificationExecutor<UsuarioBd> {

    UsuarioBd findByNombreUsuario(String username);

	List<UsuarioBd> findBySupervisorId(Long id);

}
