package com.cl.wh2listadetrabajo.repositorios;

import com.cl.wh2listadetrabajo.model.PerfilamientoReasignar;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface PerfilamientoReasignarRepositorio extends JpaRepository<PerfilamientoReasignar, String>,
        JpaSpecificationExecutor<PerfilamientoReasignar> {

}
