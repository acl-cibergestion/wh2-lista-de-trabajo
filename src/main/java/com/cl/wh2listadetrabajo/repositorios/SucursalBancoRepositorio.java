package com.cl.wh2listadetrabajo.repositorios;


import org.springframework.data.jpa.repository.JpaRepository;

import com.cl.wh2listadetrabajo.model.Sucursal;

public interface SucursalBancoRepositorio extends JpaRepository<Sucursal, Long>{

}
