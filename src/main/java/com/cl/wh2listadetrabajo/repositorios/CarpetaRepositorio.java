package com.cl.wh2listadetrabajo.repositorios;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import com.cl.wh2listadetrabajo.model.Carpeta;



public interface CarpetaRepositorio extends JpaRepository<Carpeta, Long>, JpaSpecificationExecutor<Carpeta> {
	
	@Query("select distinct c.canal, c.flujo from Carpeta c join c.usuario u where u.nombreUsuario=?1")
    List<Object[]> findDistinctFlujoCanalByUser(String nombreUsuario);
	
}
