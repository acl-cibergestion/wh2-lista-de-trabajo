package com.cl.wh2listadetrabajo.repositorios;


import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cl.wh2listadetrabajo.model.CustomTaskDefinition;

public interface CustomTaskDefinitionRepositorio extends JpaRepository<CustomTaskDefinition, Long>{

	Optional<CustomTaskDefinition> findByProcDefKeyAndTaskKey(String string, String taskDefKey);

	Boolean existsByProcDefKeyAndTaskKey(String procDefKey, String taskKey);


}
