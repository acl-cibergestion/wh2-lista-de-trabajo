package com.cl.wh2listadetrabajo.repositorios;


import org.springframework.data.jpa.repository.JpaRepository;

import com.cl.wh2listadetrabajo.model.Flujo;

import java.util.Optional;

public interface FlujoRepositorio extends JpaRepository<Flujo, Long>{

    Optional<Flujo> findByDescripcion(String descripcion);

}
