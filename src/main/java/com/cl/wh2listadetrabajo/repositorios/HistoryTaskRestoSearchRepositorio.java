package com.cl.wh2listadetrabajo.repositorios;

import com.cl.wh2listadetrabajo.model.HistoryTaskRestoSearch;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface HistoryTaskRestoSearchRepositorio extends JpaRepository<HistoryTaskRestoSearch, String>,
        JpaSpecificationExecutor<HistoryTaskRestoSearch> {

	List<HistoryTaskRestoSearch> findByCarpetaId(long parseLong, Pageable pageable);
	
    
}
