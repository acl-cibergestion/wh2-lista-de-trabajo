package com.cl.wh2listadetrabajo.repositorios;


import org.springframework.data.jpa.repository.JpaRepository;

import com.cl.wh2listadetrabajo.model.Canal;

public interface CanalRepositorio extends JpaRepository<Canal, Long>{

}
