package com.cl.wh2listadetrabajo.repositorios;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import com.cl.wh2listadetrabajo.model.FoContabilizacion;

public interface FoContabilizacionRepositorio extends JpaRepository<FoContabilizacion, Long>, 
													JpaSpecificationExecutor<FoContabilizacion> {

	@Query("select cont from FoContabilizacion cont join cont.formalizacion f join f.carpeta c where c.id=?1")
    FoContabilizacion findFoContabilzacionByCarpeta(Long carpetaId);

}
