package com.cl.wh2listadetrabajo.repositorios;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import com.cl.wh2listadetrabajo.model.CaCredito;
import com.cl.wh2listadetrabajo.model.DestinoCredito;

public interface CaCreditoRepositorio extends JpaRepository<CaCredito, Long>, 
													JpaSpecificationExecutor<CaCredito> {

	@Query("select distinct d from CaCredito cc join cc.destino d join cc.carpeta c join c.usuario u where u.nombreUsuario=?1")
    List<DestinoCredito> findDistinctByUsuario(String nombreUsuario);

	List<CaCredito> findByCarpetaId(Long valueOf);

}
