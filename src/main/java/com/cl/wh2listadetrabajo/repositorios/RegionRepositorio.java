package com.cl.wh2listadetrabajo.repositorios;


import org.springframework.data.jpa.repository.JpaRepository;

import com.cl.wh2listadetrabajo.model.Region;

public interface RegionRepositorio extends JpaRepository<Region, Long>{

}
