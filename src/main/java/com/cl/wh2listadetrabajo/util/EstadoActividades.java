package com.cl.wh2listadetrabajo.util;

public enum EstadoActividades {
	
	ACTIVE("A"),
    COMPLETED("F"),
    INTERNALLY_TERMINATED("E");

    public final String label;

    private EstadoActividades(String label) {
        this.label = label;
    }

}
