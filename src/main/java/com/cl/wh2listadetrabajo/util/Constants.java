package com.cl.wh2listadetrabajo.util;

public class Constants {
    private Constants(){

    }

    public static final String SCHEMA="dbo";
    public static final String CONST_USER_ID = "usuarios";
    public static final String CONST_PAGE="page";
    public static final String CONST_SIZE="size";
    public static final String CONST_USERNAME="username";
    public static final String CONST_IDS="ids";
    public static final String CONST_FILTERS = "filters";
    public static final String CONST_CARPETA_ID="carpetaId";
    public static final String CONST_INSTITUCION_ID="institucionId";
    public static final String CONST_COLUMN_CARPETA_ID=" id_carpeta_col_0_0_ = ";
    public static final String CONST_COLUMN_RUT=" rut_pass_col_4_0_ = ";
    public static final String CONST_COLUMN_SEGMENTO_ATENCION_ID=" segmento_atencion_id = ";
    public static final String CONST_COLUMN_CANAL_ID=" canal_id = ";
    public static final String CONST_COLUMN_DESTINO_ID=" destino_id = ";
    public static final String CONST_COLUMN_FLUJO_ID=" flujo_id = ";
    public static final String CONST_COLUMN_REGION_ID=" region_id = ";
    public static final String CONST_COLUMN_NOMBRE=" nombre_col_5_0_ LIKE ";
    public static final String CONST_COLUMN_APELLIDO_PRIMERO=" apellido1_col_6_0_ LIKE ";
    public static final String CONST_COLUMN_APELLIDO_SEGUNDO=" apellido2_col_7_0_ LIKE ";
    public static final String CONST_COLUMN_ACTIVIDAD=" name_col_8_0_ = ";
    public static final String CONST_COLUMN_DATE_RUNNING=" due_date_runningtas = ";
    public static final String CONST_COLUMN_USERS=" USUARIO_CREADOR_ID IN (:usuarios)";
    public static final String CONST_ESTANDAR_FILTER="filtroEstandar";
    public static final String LOG_ERROR = "ERROR: {}{}";

    public static final String QUERY_LIST_WORK= "select id_carpeta_col_0_0_ as NumCarpeta," + "NUMERO_PCF_col_1_0_ as NumPCF," + "PRIORIDAD_col_2_0_ as Prioridad," + "descripcion_col_3_0_ as MotivoPrioridad," + "RUT_PASS_col_4_0_ as Rut," + "NOMBRE_col_5_0_ as NombreCliente," + "APELLIDO1_col_6_0_ as ApellidoPaterno," + "APELLIDO2_col_7_0_ as ApellidoMaterno," + "NAME_col_8_0_ as NombreActividad," + "CREATE_TIME_ as FechaInicio," + "DUE_DATE_col_9_0_ as FechaEsperada," + "T_ESTANDAR_D_col_11_0_ as EE," + "FORM_KEY_col_12_0_ as FormularioId," + "CHECKLIST_KEY_ as CheckListId," + "DUE_DATE_runningtas as DentroEstandar," + "ID_runningtas_col_16_0_ as ActividadId," + "id_col_17_0_ as TipoOperacionId," + "ID_col_18_0_ as RunningExecutionId," + "TASK_DEF_KEY_col_20_0_ as TaskDefKey," + "PROC_DEF_ID_col_21_0_ as ProcDefId," + "USUARIO_CREADOR_ID as UserId, " + "CANAL_ID as canalId, " + "FLUJO_ID as flujoId, "+ "SUCURSAL_BANCO_ID as sucursalId, " + "REGION_ID as regionId, " + "SEGMENTO_ATENCION_ID as segmentoId, " + "DESTINO_ID as destinoId, " +  "END_TIME as endTime " + "FROM SURA_WFC_DS_M2.dbo.lista_tabajo_view ";
    public static final String QUERY_COUNT_LIST_WORK="SELECT COUNT(*) " +
    "FROM [SURA_WFC_DS_M2].[dbo].[lista_tabajo_view] ";

    public static final String QUERY_COUNT_RUT_LIST_WORK="SELECT COUNT(DISTINCT RUT_PASS_col_4_0_) " +
            "FROM [SURA_WFC_DS_M2].[dbo].[lista_tabajo_view] ";


}
