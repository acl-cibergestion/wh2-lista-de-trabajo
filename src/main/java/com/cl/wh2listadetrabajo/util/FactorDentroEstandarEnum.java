package com.cl.wh2listadetrabajo.util;

public enum FactorDentroEstandarEnum {

    FACTOR(Long.valueOf(70));

    private Long id;

    FactorDentroEstandarEnum(Long id) {
        this.id = id;
    }

    public Long get() {
        return this.id;
    }
}
