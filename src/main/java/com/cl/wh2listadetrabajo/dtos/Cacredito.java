package com.cl.wh2listadetrabajo.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Cacredito {

	private Long id;
	private String numOperacion;
	private Usuario notaria;
}
