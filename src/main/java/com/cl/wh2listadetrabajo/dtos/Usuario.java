package com.cl.wh2listadetrabajo.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Usuario {
	
	private Long id;

    private String primerNombre;

    private String apellidoPaterno;
    
    private String nombreCompleto;
    
    private String nombreUsuario;

}
