package com.cl.wh2listadetrabajo.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
@NoArgsConstructor
public class CustomTaskDefinition {


    private String id;
    private String procDefKey;
    private String taskKey;
    private String checklistKey;
    private String formKey;
    private Boolean rehacerDesdeEsta;
    private Boolean rehacerHaciaEsta;
    private Boolean avanceMasivo;
    private String tipoTarea;
    private String etapa;
    private Integer orden;
    private Integer referenciaOrden;
    private Boolean esSatelite;
    private String name;

}
