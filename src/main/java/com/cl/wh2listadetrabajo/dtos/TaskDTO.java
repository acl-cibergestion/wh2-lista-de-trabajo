package com.cl.wh2listadetrabajo.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class TaskDTO implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    private String idTask;
    
    private String taskDefKey;
    
    private String procDefKey;

    private String actNom;

    private String idExecution;

    private Long carpetaId;

    private String responsableNombre;
    
    private String fechaCreacion;
    
    private RolDTO rol;

}
