package com.cl.wh2listadetrabajo.dtos;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class HistoricoResponse {

	private String id;

    private String tarea;

    private String taskDefKey;

    private String e;

    private String fechaInicio;

    private String fechaEsperada;

    private String fechaTermino;

    private String responsable;

    private String estandarD;

    private String estandarH;

    private String realD;

    private String realH;

    private String description;

    private String operacion;

    private String flujo;

    private boolean indActivo;

    private String dentroEstandar;

    private boolean selectedDelete;

    private Long tipoOperacion;


    /**
     * @param id
     * @param tarea
     * @param e
     * @param startDate
     * @param dueDate
     * @param endDate
     * @param responsable
     * @param estandarD
     * @param estandarH
     * @param realD
     * @param realH
     * @param description
     * @param indEstandar
     * @param operacion
     * @param tipoAux
     * @param flujo
     * @param tipoOperacion
     */
    public HistoricoResponse(String id, String tarea, String taskDefKey, String e, Date startDate, Date dueDate, Date endDate,
                              String responsable, int estandarD, int estandarH, int realD, int realH,
                              String description, String dentroEstandar, Long operacion, String tipoAux, String flujo, Long tipoOperacion) {
        this.id = id;
        this.tarea = tarea;
        this.taskDefKey = taskDefKey;
        this.e = (e.equals("completed") || e.equals("deleted")) ? "F" : e;

        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");

        this.fechaInicio = startDate == null ? "" : dateFormat.format(startDate);
        this.fechaEsperada = dueDate == null ? "" : dateFormat.format(dueDate);
        this.fechaTermino = endDate == null ? "" : dateFormat.format(endDate);
        this.responsable = responsable;
        this.estandarD = Integer.toString(estandarD);
        this.estandarH = Integer.toString(estandarH);
        this.realD = Integer.toString(realD);
        this.realH = Integer.toString(realH);
        this.description = description;
        this.dentroEstandar = dentroEstandar;

        if (operacion != null) {
            this.operacion = Long.toString(operacion);
        } else {
            this.operacion = "";
        }

        if ("principal".equals(tipoAux)) {
            this.flujo = flujo;
        } else {
            this.flujo = flujo.concat(" [Op. ").concat(this.operacion).concat("]");
        }

        this.tipoOperacion = tipoOperacion;

        this.indActivo = "A".equals(e);
        this.selectedDelete = false;
    }

}
