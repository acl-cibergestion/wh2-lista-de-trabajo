package com.cl.wh2listadetrabajo.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
@JsonIgnoreProperties(ignoreUnknown = true)
public class TareasPendientesDTO {
	
	private Long flujoId;
	private String flujo;
	private String taskDefKey;
	private String tarea;
	private Integer cantidad;
	private boolean avanceMasivo;
}
