package com.cl.wh2listadetrabajo.dtos;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class Institucion {

    private Long id;

    private String razonSocial;
    
    private Boolean activo;
    
    private String codigo;

}
