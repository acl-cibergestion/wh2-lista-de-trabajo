package com.cl.wh2listadetrabajo.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Persona {
	
	private Long id;

    private String nombres;

    private String apellido1;
    
    private String apellido2;
    
    private String rut;
    
    private String nombreCompleto;
    	

}
