package com.cl.wh2listadetrabajo.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
public class RolDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    
    private String descripcion;

}
