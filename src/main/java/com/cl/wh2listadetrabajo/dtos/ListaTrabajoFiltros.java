package com.cl.wh2listadetrabajo.dtos;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class ListaTrabajoFiltros {

	private List<FilterFlujoCanal> canales;
	private List<String> actividades;
	private List<SegmentoDTO> segmentos;
	private List<DestinoCreditoDTO> destinos;
	private List<RegionDTO> regiones;
	private List<SucursalBanco> sucursales;
}
