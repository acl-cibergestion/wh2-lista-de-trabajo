package com.cl.wh2listadetrabajo.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class RunningTaskDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Date dueDate;
    
    private String taskDef;
    
    private String taskDefKey;
    
    private String processDef;
    
    private Long processDefKey;
    
    private String dentroEstandar;
    
    
}
