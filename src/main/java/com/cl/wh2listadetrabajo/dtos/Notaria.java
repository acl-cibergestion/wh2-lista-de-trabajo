package com.cl.wh2listadetrabajo.dtos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Notaria {
	
	private Long id;
	private String descripcion;
}
