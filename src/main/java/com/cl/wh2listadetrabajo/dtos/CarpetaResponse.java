package com.cl.wh2listadetrabajo.dtos;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class CarpetaResponse {
	
	private Long id;
	private Date fechaIngreso;
	private String fechaFormarteada;
	private Canal canal;
	private Flujo flujo;
	private EstadoCarpeta estado;
	private SucursalBanco sucursalBanco;
	private Persona persona;
	private Servicio servicio;
	private Cacredito credito;
	private Institucion institucion;
	private BigDecimal numPCF; 
	private String codeActivity; 

}
