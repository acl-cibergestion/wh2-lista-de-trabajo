package com.cl.wh2listadetrabajo.dtos;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class FilterFlujoCanal implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long idFlujo;
    private Long idCanal;
    private String flujoCanal;

    public FilterFlujoCanal(Long idFlujo, Long idCanal, String flujoCanal) {
        this.idFlujo = idFlujo;
        this.idCanal = idCanal;
        this.flujoCanal = flujoCanal;
    }
}
