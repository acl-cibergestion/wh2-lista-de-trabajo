package com.cl.wh2listadetrabajo.dtos;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
@JsonIgnoreProperties(ignoreUnknown = true)
public class FlujosPendientesDTO {
	
	private Long flujoId;
	private String flujo;
	private List<TareasPendientesDTO> tareas;
	
}
