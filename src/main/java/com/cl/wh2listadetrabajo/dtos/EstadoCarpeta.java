package com.cl.wh2listadetrabajo.dtos;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class EstadoCarpeta {

    private Long id;

    private String nombre;

    private Boolean activo;

}
