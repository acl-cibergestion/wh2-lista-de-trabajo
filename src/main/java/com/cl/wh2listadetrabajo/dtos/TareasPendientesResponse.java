package com.cl.wh2listadetrabajo.dtos;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
@JsonIgnoreProperties(ignoreUnknown = true)
public class TareasPendientesResponse {
	
	private List<FlujosPendientesDTO> flujos;
	Integer countDentroEstandar;
	Integer countDentroEstandarLimite;
	Integer countFueraEstandar;
	Integer countTotal;
	
}
