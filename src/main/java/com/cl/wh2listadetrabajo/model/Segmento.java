package com.cl.wh2listadetrabajo.model;


import com.cl.wh2listadetrabajo.util.Constants;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

import javax.persistence.*;

/**
 * The persistent class for the CG_MOD_SEGMENTOS database table.
 *
 */
@Entity
@Table(name = "CON_SEGMENTOS", schema = Constants.SCHEMA)
@Getter
@Setter
@NoArgsConstructor
@ToString
public class Segmento implements Serializable {

    private static final long serialVersionUID = 1L;

	@Id
    private Long id;

    private String nombre;

    private Boolean activo;

}
