package com.cl.wh2listadetrabajo.model;

import com.cl.wh2listadetrabajo.util.Constants;

import lombok.Getter;
import lombok.Setter;


import javax.persistence.*;
import java.io.Serializable;


@Entity
@Table(name = "ACT_RE_PROCDEF", schema = Constants.SCHEMA)
@Getter
@Setter
public class ProcessDefinition implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
    @Column(name = "id_")
    private String id;

	@Column(name = "NAME_")
    private String name;
	
    @Column(name = "KEY_")
    private String key;

    
}
