package com.cl.wh2listadetrabajo.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class PaginadoResponse {
    int totalPages;
    long totalElement;
    long totalClientes;
    List<?> items;

    public PaginadoResponse(int totalPages, List<?> items,long totalElement, long totalClientes) {
        this.totalPages = totalPages;
        this.items = items;
        this.totalElement=totalElement;
        this.totalClientes=totalClientes;
    }
    
    public PaginadoResponse(int totalPages, List<?> items,long totalElement) {
        this.totalPages = totalPages;
        this.items = items;
        this.totalElement=totalElement;
    }
}
