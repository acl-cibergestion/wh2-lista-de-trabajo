package com.cl.wh2listadetrabajo.model;

import com.cl.wh2listadetrabajo.util.Constants;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "ACT_HI_PROCINST", schema = Constants.SCHEMA)
@Getter
@Setter
public class HistoryProcessInstance {

    @Id
    @Column(name = "id_" )
    private String id;

    @ManyToOne
    @JoinColumn(name = "BUSINESS_KEY_", referencedColumnName = "ID")
    private Carpeta carpeta;

    @Column(name = "STATE_")
    private String state;


}
