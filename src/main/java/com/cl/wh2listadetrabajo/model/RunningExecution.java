/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cl.wh2listadetrabajo.model;

import com.cl.wh2listadetrabajo.util.Constants;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "ACT_RU_EXECUTION", schema = Constants.SCHEMA)
@Getter
@Setter
public class RunningExecution implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;
    // @Max(value=?) @Min(value=?)//if you know range of your decimal fields
    /**
     * The id.
     */
    // consider using these annotations to enforce field validation
    @Id
    @NonNull
    @Column(name = "ID_")
    private String id;

    /**
     * The act id.
     */
    @Column(name = "ACT_ID_")
    private String actId;

    /**
     * The carpeta.
     */
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "BUSINESS_KEY_", referencedColumnName = "ID")
    private Carpeta carpeta;

}
