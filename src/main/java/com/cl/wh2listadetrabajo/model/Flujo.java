package com.cl.wh2listadetrabajo.model;

import com.cl.wh2listadetrabajo.util.Constants;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;


/**
 * The persistent class for the WFH_FLUJOS database table.
 * 
 */
@Entity
@Table(name="WFH_FLUJOS", schema = Constants.SCHEMA)
@Getter
@Setter
public class Flujo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;
	private String descripcion;
	
	@Column(name="PROC_DEF_ID_")
	private String procDefId;

	
}
