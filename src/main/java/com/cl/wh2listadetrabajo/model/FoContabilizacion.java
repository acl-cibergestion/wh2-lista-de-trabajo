package com.cl.wh2listadetrabajo.model;

import com.cl.wh2listadetrabajo.util.Constants;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the WFH_FO_CONTABILIZACIONES database table.
 * 
 */
@Entity
@Table(name="WFH_FO_CONTABILIZACIONES", schema = Constants.SCHEMA)
@Getter
@Setter
public class FoContabilizacion {

	@Id
	private Long id;

	@Column(name="NUMERO_PCF")
	private BigDecimal numeroPcf;

	@OneToOne
	@JoinColumn(name="FORMALIZACION_ID")
	private Formalizacion formalizacion;
}