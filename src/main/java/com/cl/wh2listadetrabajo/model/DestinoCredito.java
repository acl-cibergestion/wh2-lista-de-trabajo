package com.cl.wh2listadetrabajo.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import com.cl.wh2listadetrabajo.util.Constants;
import javax.persistence.*;

/**
 * The persistent class for the CG_MOD_DESTINOS_CREDITO database table.
 *
 */
@Entity
@Table(name = "CON_DESTINOS_CREDITO", schema = Constants.SCHEMA)
@Getter
@Setter
@NoArgsConstructor
@ToString
public class DestinoCredito {

    @Id
    private Long id;
    private String descripcion;

    private Boolean activo;

}
