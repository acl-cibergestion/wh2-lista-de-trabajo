package com.cl.wh2listadetrabajo.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


import javax.persistence.*;

import com.cl.wh2listadetrabajo.util.Constants;

import java.io.Serializable;

@Entity
@Table(name = "CON_ESTADOS_CARPETA", schema = Constants.SCHEMA)
@Getter
@Setter
@NoArgsConstructor
public class EstadoCarpeta implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private Long id;

    private String nombre;

    private Boolean activo;

}
