package com.cl.wh2listadetrabajo.model;

import com.cl.wh2listadetrabajo.util.Constants;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@Entity
@Table(name="WFH_FO_CHECKLISTS", schema = Constants.SCHEMA)

public class FoChecklist implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	@OneToOne
	@JoinColumn(name="OPCION_ID")
	private ChOpcion opcion;

	@OneToOne
	@JoinColumn(name="FORMALIZACION_ID")
	private Formalizacion formalizacion;

	
}