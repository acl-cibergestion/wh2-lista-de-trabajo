package com.cl.wh2listadetrabajo.model;

import com.cl.wh2listadetrabajo.util.Constants;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * The persistent class for the WFH_FORMALIZACIONES database table.
 */
@Entity
@Table(name = "WFH_FORMALIZACIONES", schema = Constants.SCHEMA)
@Getter
@Setter
public class Formalizacion implements Serializable {

    private static final long serialVersionUID = 1L;

	@Id
    private Long id;

    @OneToOne
    @JoinColumn(name = "SEGMENTO_ATENCION_ID")
    private Segmento segmentoAtencion;

    @OneToOne
    @JoinColumn(name = "CARPETA_ID")
    private Carpeta carpeta;

}
