package com.cl.wh2listadetrabajo.model;

import javax.persistence.*;

import com.cl.wh2listadetrabajo.util.Constants;

import lombok.Getter;
import lombok.Setter;


@Entity
@Table(name = "ACT_HI_TASKINST_RESTO", schema = Constants.SCHEMA)
@Getter
@Setter
public class HistoryTaskResto {

    @Id
    @Column(name = "id_" )
    private String id;

    @Column(name = "T_REAL_H", precision = 4)
    private int tRealH;
    
    @Column(name = "T_ESTANDAR_H", precision = 4)
    private int tEstandarH;
    
    @Column(name = "T_ESTANDAR_D" , precision = 4)
    private int tEstandarD;


    @Column(name = "T_REAL_D" , precision = 4)
    private int tRealD;

    @Column(name = "OPERACION_ID", insertable = false, updatable = false)
    private Long carpetaId;
    
    @Column(name = "CONTADOR" , precision = 4)
    private int contador;

    @Column(name = "DELETE_REASON_OLD" )
    private String deleteReasonOld;
    
    @OneToOne
	@JoinColumn(name = "OPERACION_ID")
    private Carpeta carpeta;

    @OneToOne(fetch = FetchType.LAZY)
    @PrimaryKeyJoinColumn
    private RunningTask runningTask;

        
}
