package com.cl.wh2listadetrabajo.model;

import com.cl.wh2listadetrabajo.util.Constants;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * The persistent class for the CG_MOD_TIPOS_INSTITUCION database table.
 *
 */
@Entity
@Table(name = "CON_TIPOS_INSTITUCION", schema = Constants.SCHEMA)
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class TipoInstitucion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private Long id;

    private String nombre;

    private Boolean activo;
}
