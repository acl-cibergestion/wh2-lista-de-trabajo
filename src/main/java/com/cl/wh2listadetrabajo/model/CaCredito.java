package com.cl.wh2listadetrabajo.model;

import com.cl.wh2listadetrabajo.util.Constants;

import lombok.Getter;
import lombok.Setter;


import javax.persistence.*;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

/**
 * The persistent class for the WFH_CA_CREDITOS database table.
 *
 */
@Entity
@Table(name = "WFH_CA_CREDITOS", schema = Constants.SCHEMA)
@Getter
@Setter
public class CaCredito {

    @Id
    private Long id;

    @OneToOne
    @JoinColumn(name = "CARPETA_ID")
    private Carpeta carpeta;
    
    @Column(name = "CARPETA_ID" , updatable = false , insertable = false)
    private Long carpetaId;

    @OneToOne
    @JoinColumn(name = "DESTINO_ID")
    private DestinoCredito destino;
    
    @Column(name = "NUM_OPERACION")
    private String numOperacion;
    
    @OneToOne
    @JoinColumn(name = "NOTARIA_ID", referencedColumnName = "ID" , updatable = false , insertable = false, nullable = true)
    @NotFound(action = NotFoundAction.IGNORE)
    private UsuarioBd notaria;

    
}
