package com.cl.wh2listadetrabajo.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

import java.io.Serializable;

import static com.cl.wh2listadetrabajo.util.Constants.SCHEMA;


/**
 * The persistent class for the WFH_USUARIOS database table.
 */
@Entity
@Table(name = "WFH_USUARIOS", schema = SCHEMA)
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class UsuarioBd implements Serializable {

    private static final long serialVersionUID = 4L;

    @Id
    private Long id;

    @Column(name = "NOMBRE_USUARIO")
    private String nombreUsuario;
    
    @Column(name = "APELLIDO_PATERNO")
    private String apellidoPaterno;
    
    @Column(name = "APELLIDO_MATERNO")
    private String apellidoMaterno;
    
    @Column(name = "PRIMER_NOMBRE")
    private String primerNombre;
    
    @Column(name = "SEGUNDO_NOMBRE")
    private String segundoNombre;

    @Column(name = "SUPERVISOR")
    private Boolean isSupervisor;
    
    @Column(name = "ADMIN_SISTEMA")
    private Boolean adminSistema;
    
    @Column(name = "SUPERVISOR_ID")
    private Long supervisorId;

}
