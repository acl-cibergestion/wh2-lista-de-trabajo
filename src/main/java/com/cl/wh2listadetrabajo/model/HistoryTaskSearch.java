package com.cl.wh2listadetrabajo.model;

import java.util.Date;

import javax.persistence.*;

import com.cl.wh2listadetrabajo.util.Constants;

import lombok.Getter;
import lombok.Setter;


@Entity
@Table(name = "ACT_HI_TASKINST", schema = Constants.SCHEMA)
@Getter
@Setter
public class HistoryTaskSearch {

	@Id
	@Column(name = "id_", length = 255)
    private String id;
	
    @Column(name = "name_", length = 255)
    private String name;

    @OneToOne
	@JoinColumn(name = "assignee_")
    private UsuarioBd usuario;
    
    @Column(name = "START_TIME_")
    private Date startTime;

    @Column(name = "END_TIME_")
    private Date endTime;

    @Column(name = "DUE_DATE_")
    private Date dueDate;

    @Column(name = "TASK_DEF_KEY_")
    private String taskDefKey;

    @Column(name = "DELETE_REASON_")
    private String deleteReason;
    
    @OneToOne(fetch = FetchType.LAZY)
    @PrimaryKeyJoinColumn
    private HistoryTaskRestoSearch historyTaskResto;
}
