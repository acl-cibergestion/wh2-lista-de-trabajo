package com.cl.wh2listadetrabajo.model;

import com.cl.wh2listadetrabajo.util.Constants;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;


/**
 * The persistent class for the WFH_CANALES database table.
 */
@Entity
@Table(name = "WFH_CANALES", schema = Constants.SCHEMA)
@Getter
@Setter
public class Canal implements Serializable {

    private static final long serialVersionUID = 5L;

    @Id
    private Long id;

    private String descripcion;

}
