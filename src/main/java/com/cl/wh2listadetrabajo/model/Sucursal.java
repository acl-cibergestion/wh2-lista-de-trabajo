package com.cl.wh2listadetrabajo.model;


import com.cl.wh2listadetrabajo.util.Constants;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "CON_SUCURSALES", schema = Constants.SCHEMA)
@Getter
@Setter
@NoArgsConstructor
public class Sucursal implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private Long id;

    @Column(name = "GLOSA")
    private String glosa;

    private String descripcion;
    
    @JoinColumn(name = "INSTITUCION_ID")
    @OneToOne
    private Institucion institucion;

    private Boolean activo;

}
