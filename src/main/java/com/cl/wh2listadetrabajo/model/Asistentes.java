package com.cl.wh2listadetrabajo.model;

import com.cl.wh2listadetrabajo.util.Constants;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "WFH_ASISTENTES", schema = Constants.SCHEMA)
@SequenceGenerator(allocationSize = 1, name = "SQ_ID_ASISTENTES_GEN", sequenceName = "WFC_SEC_ID_ASISTENTES")
@Getter
@Setter
@NoArgsConstructor
public class Asistentes implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_ID_ASISTENTES_GEN")
    private Long id;

    @Column(name = "USUARIO_ID")
    private Long usuario;

	@Column(name = "ASISTENTE_ID")
    private Long asistente;

}
