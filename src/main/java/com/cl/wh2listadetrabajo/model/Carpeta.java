package com.cl.wh2listadetrabajo.model;

import com.cl.wh2listadetrabajo.util.Constants;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "WFH_CARPETAS", schema = Constants.SCHEMA)
@Getter
@Setter
@NoArgsConstructor
public class Carpeta implements Serializable {
    private static final long serialVersionUID = 2L;

    @Id
    private Long id;

    @OneToOne
    @JoinColumn(name = "USUARIO_CREADOR_ID")
    private UsuarioBd usuario;
    
    @OneToOne
    @JoinColumn(name = "CANAL_ID")
    private Canal canal;
    
    @OneToOne
    @JoinColumn(name = "FLUJO_ID")
    private Flujo flujo;
    
    @Column(name = "CARPETA_ID")
    private Long carpetaPadre;
    
    @Column(name = "TIPO_OPERACION_ID")
    private Long tipoOperacionId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_INGRESO")
    private Date fechaIngreso;

    @OneToOne
    @JoinColumn(name = "SUCURSAL_BANCO_ID")
    private Sucursal sucursalBanco;
    
    @OneToOne
    @JoinColumn(name = "ESTADO_ID")
    private EstadoCarpeta estadoCarpeta;
}
