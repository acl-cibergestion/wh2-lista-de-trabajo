package com.cl.wh2listadetrabajo.model;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class ListaTrabajo {
    private Long id;
    private String actividadId;
    private Long numCarpeta;
    private Long numPCF;
    private Long prioridad;
    private String motivoPrioridad;
    private String rut;
    private Integer rutValor;
    private String nombreCliente;
    private String nombreActividad;
    private String fechaInicio;
    private String fechaEsperada;
    private Integer eE;
    private Boolean formulario;
    private Boolean checklist;
    private Long dentroEstandar;
    private Boolean avanzarMasivo;
    private Long tipoOperacionId;
    private String formularioId;
    private String checklistId;
    private String runningExecutionId;
    private String taskDefKey;
    private String procDefId;
    private Long canalId;
    private Long flujoId;
    private Long sucursalId;
    private Long regionId;
    private Long segmentoId;
    private Long destinoId;



    public ListaTrabajo(String actividadId, Long numCarpeta,
                        Long numPCF, Long prioridad, String motivoPrioridad,
                        String rut, String nombreCliente,
                        String apellidoPaterno,String apellidoMaterno,
                        String nombreActividad, String fechaInicio,
                        String fechaEsperada, Integer eE, String dentroEstandar,
                        Long tipoOperacionId,String formularioId, String checklistId,
                        String runningExecutionId,
                        String taskDefKey, String procDefId,
                        Long canalId, Long flujoId, Long sucursalId,
                        Long regionId, Long segmentoId, Long destinoId) {
        this.numCarpeta = numCarpeta;
        this.numPCF = numPCF;
        this.prioridad = prioridad;
        this.motivoPrioridad = motivoPrioridad;
        this.rut = rut;
        this.rutValor=0;
        this.nombreCliente = (nombreCliente + " " + apellidoPaterno + " " + apellidoMaterno);
        this.nombreActividad = nombreActividad;
        this.fechaInicio = fechaInicio;
        this.fechaEsperada = fechaEsperada;
        this.eE = eE;
        this.formulario = !("".equals(formularioId) || formularioId == null);
        this.checklist = !("".equals(checklistId) || checklistId == null);
        this.formularioId = formularioId;
        this.checklistId = checklistId;
        this.dentroEstandar = Long.valueOf(dentroEstandar);
        this.actividadId = actividadId;
        this.tipoOperacionId = tipoOperacionId;
        this.runningExecutionId = runningExecutionId;
        this.taskDefKey = taskDefKey;
        this.procDefId = procDefId;
        this.canalId = canalId;
        this.flujoId = flujoId;
        this.sucursalId = sucursalId;
        this.regionId = regionId;
        this.segmentoId = segmentoId;
        this.destinoId = destinoId;
    }
}
