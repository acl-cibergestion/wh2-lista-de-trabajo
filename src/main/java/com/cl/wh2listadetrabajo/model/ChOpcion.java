package com.cl.wh2listadetrabajo.model;

import com.cl.wh2listadetrabajo.util.Constants;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@Entity
@Table(name = "WFH_CH_OPCIONES", schema = Constants.SCHEMA)
public class ChOpcion implements Serializable {

    private static final long serialVersionUID = 1L;

	@Id
    private Long id;

    @OneToOne
    @JoinColumn(name = "BLOQUE_ID")
    private Checklist checklist;


}