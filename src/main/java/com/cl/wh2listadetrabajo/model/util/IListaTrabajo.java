package com.cl.wh2listadetrabajo.model.util;

import java.math.BigDecimal;

public interface IListaTrabajo {
    Long getNumCarpeta();
    BigDecimal getNumPCF();
    Long getPrioridad();
    String getMotivoPrioridad();
    String getRut();
    String getNombreCliente();
    String getApellidoPaterno();
    String getApellidoMaterno();
    String getNombreActividad();
    String getFechaInicio();
    String getFechaEsperada();
    Integer getEE();
    String getFormularioId();
    String getCheckListId();
    String getDentroEstandar();
    String getActividadId();
    Long getTipoOperacionId();
    String getRunningExecutionId();
    String getTaskDefKey();
    String getProcDefId();
    Long getUserId();
}
