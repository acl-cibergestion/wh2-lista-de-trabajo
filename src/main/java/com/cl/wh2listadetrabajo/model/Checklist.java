package com.cl.wh2listadetrabajo.model;

import com.cl.wh2listadetrabajo.util.Constants;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

import javax.persistence.*;


@Entity
@Table(name = "WFH_CHECKLISTS", schema = Constants.SCHEMA)
@Setter
@Getter
public class Checklist implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
    private Long id;

    @Column(name = "ACTIVITI_ID")
    private String codigoActiviti;

    
}
