package com.cl.wh2listadetrabajo.model;

import com.cl.wh2listadetrabajo.util.Constants;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;


/**
 * The persistent class for the WFH_PERFILAMIENTO_REASIGNAR database table.
 * 
 */
@Entity
@Table(name="WFH_PERFILAMIENTO_REASIGNAR", schema = Constants.SCHEMA)
@SequenceGenerator(allocationSize = 1, name = "SQ_ID_PERFILAMIENTO_REASIGNAR_GEN", sequenceName = "WFH_SEC_ID_PERFIL_REASIGNAR")
@Getter
@Setter
public class PerfilamientoReasignar implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_ID_PERFILAMIENTO_REASIGNAR_GEN")
	private Long id;

	@JoinColumn(name = "INSTITUCION_ID", referencedColumnName = "ID", updatable = false, insertable = false)
	@ManyToOne
	private Institucion institucion;

	@Column(name = "TIPO_INSTITUCION")
	private Boolean permisoTipoInstitucion;

 	@Column(name = "INSTITUCION")
	private Boolean permisoInstitucion;

	@Column(name = "ROL")
	private Boolean permisoRol;

	@Column(name = "RESPONSABLE")
	private Boolean permisoResponsable;

}