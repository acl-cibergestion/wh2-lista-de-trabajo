package com.cl.wh2listadetrabajo.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

import com.cl.wh2listadetrabajo.util.Constants;

import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "ACT_RU_TASK_RESTO", schema = Constants.SCHEMA)
@Getter
@Setter
@NoArgsConstructor
@ToString
public class RunningTaskResto implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID_")
    private String id;

    @Column(name = "T_ESTANDAR_D")
    private int tEstandarD;

    @Column(name = "T_ESTANDAR_H")
    private int tEstandarH;

    @Column(name = "OPERACION_ID", insertable = false, updatable = false)
    private Long carpetaId;

    @Column(name = "TREE_")
    private String tree;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ROL_ID", referencedColumnName = "ID")
    private Rol rol;

    private int contador;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "WARNING_DUE_DATE_")
    private Date warningDueDate;
}

