package com.cl.wh2listadetrabajo.model;

import com.cl.wh2listadetrabajo.util.Constants;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;


/**
 * The persistent class for the WFH_ROLES database table.
 */
@Entity
@Table(name = "WFH_ROLES", schema = Constants.SCHEMA)
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Rol implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private Long id;

    @Column(name = "DESCRIPCION")
    private String descripcion;

}