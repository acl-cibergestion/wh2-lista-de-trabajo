package com.cl.wh2listadetrabajo.model;

import com.cl.wh2listadetrabajo.util.Constants;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * The persistent class for the CG_MOD_INSTITUCIONES database table.
 *
 */
@Entity
@Table(name = "CON_INSTITUCIONES", schema = Constants.SCHEMA)
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Institucion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private Long id;

    @Column(name = "RAZON_SOCIAL")
    private String razonSocial;

    //bi-directional many-to-one association to TipoInstitucion
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TIPO_INSTITUCION_ID", updatable = false, insertable = false)
    private TipoInstitucion tipoInstitucion;

    private Boolean activo;

}
