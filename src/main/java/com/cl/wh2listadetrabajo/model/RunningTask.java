package com.cl.wh2listadetrabajo.model;

import com.cl.wh2listadetrabajo.util.Constants;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "ACT_RU_TASK", schema = Constants.SCHEMA)
@Getter
@Setter
public class RunningTask implements Serializable {
   
    private static final long serialVersionUID = 1L;

	@Id
    @Column(name = "ID_")
    private String id;

    @Column(name = "REV_")
    private Long rev;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "EXECUTION_ID_", referencedColumnName = "ID_")
    private RunningExecution runningExecution;

    @OneToOne
    @JoinColumn(name = "PROC_DEF_ID_", referencedColumnName = "ID_")
    private ProcessDefinition processDefinition;

    @Column(name = "NAME_")
    private String name;

    @Column(name = "PARENT_TASK_ID_")
    private String parentTaskId;

    @Column(name = "DESCRIPTION_")
    private String description;

    @Column(name = "TASK_DEF_KEY_")
    private String taskDefKey;

    @Column(name = "OWNER_")
    private String owner;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ASSIGNEE_", referencedColumnName = "ID", updatable = false, insertable = false)
    private UsuarioBd usuario;

    @Column(name = "DELEGATION_")
    private String delegation;

    @Column(name = "PRIORITY_")
    private Long priority;

    @Column(name = "CREATE_TIME_")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createTime;

    @Column(name = "DUE_DATE_")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dueDate;

    @Column(name = "CATEGORY_")
    private String category;

    @Column(name = "SUSPENSION_STATE_")
    private Long suspensionState;

    @Column(name = "TENANT_ID_")
    private String tenantId;

    @Column(name = "FORM_KEY_")
    private String formKey;

}
