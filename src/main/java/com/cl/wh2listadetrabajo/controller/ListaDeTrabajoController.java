package com.cl.wh2listadetrabajo.controller;

import java.io.IOException;
import java.util.List;

import com.cl.wh2listadetrabajo.dtos.*;
import com.cl.wh2listadetrabajo.exceptions.CustomException;
import com.cl.wh2listadetrabajo.model.*;
import com.cl.wh2listadetrabajo.servicios.IListaDeTrabajoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api/listatrabajo")
public class ListaDeTrabajoController {

    @Autowired
    IListaDeTrabajoService iListaDeTrabajoService;
    
    @PostMapping("/findCarpeta")
    public ResponseEntity<CarpetaResponse> findCarpeta(@RequestBody(required = false) String qparams) throws IOException{
        return new ResponseEntity<> (iListaDeTrabajoService.findCarpeta(qparams), HttpStatus.OK);
    }
    
    @PostMapping("/findSucursales")
    public ResponseEntity<List<Sucursal>> findSucursales(@RequestBody(required = false) String qparams) throws CustomException{
        return new ResponseEntity<> (iListaDeTrabajoService.findSucursales(qparams), HttpStatus.OK);
    }

    @PostMapping("/flujoCanales")
    public ResponseEntity<List<FilterFlujoCanal>> findFlujoCanales(@RequestBody(required = false) String qparams) throws CustomException {
        return new ResponseEntity<> (iListaDeTrabajoService.findFlujoCanalByCarpeta(qparams), HttpStatus.OK);
    }
    
    @PostMapping("/findDestinosCredito")
    public List<DestinoCredito> findDestinos(@RequestBody(required = false) String qparams) throws CustomException{
        return iListaDeTrabajoService.findDestinos(qparams);
    }
    
    @PostMapping("/findSegmentos")
    public List<Segmento> findSegmentos(@RequestBody(required = false) String qparams) throws CustomException{
        return iListaDeTrabajoService.findSegmentos(qparams);
    }
    
    @PostMapping("/findActividades")
    public List<String> findActividades(@RequestBody(required = false) String qparams) throws CustomException{
        return iListaDeTrabajoService.findActividades(qparams);
    }

    @PostMapping("/findListaTrabajo")
    public PaginadoResponse getAllListaTrabajoFilter(@RequestBody(required = false) String qparams) throws CustomException {
        return iListaDeTrabajoService.findListaTrabajo(qparams);
    }
    
    @PostMapping("/findHistorico")
    public List<HistoricoResponse> findHistoricoActividades(@RequestBody(required = false) String qparams) throws CustomException {
        return iListaDeTrabajoService.findHistoricoActividades(qparams);
    }

    @PostMapping("/findActividad")
    public String findActividad(@RequestBody(required = false) String qparams) throws CustomException{
        return iListaDeTrabajoService.findActividad(qparams);
    }

    @PostMapping("/findListActividades")
    public List<TaskDTO> findListActividad(@RequestBody(required = false) String qparams) throws CustomException{
        return iListaDeTrabajoService.findListActividades(qparams);
    }

    @PostMapping("/perfilamientoReasignar")
    public PerfilamientoReasignar perfilamientoReasignar(@RequestBody(required = false) String qparams) throws CustomException{
        return iListaDeTrabajoService.findByInstitucion(qparams);
    }
    
    @PostMapping("/findRegiones")
    public List<Region> findRegiones(@RequestBody(required = false) String qparams) throws CustomException{
        return iListaDeTrabajoService.findRegiones(qparams);
    }
    
    @PostMapping("/findFiltros")
    public ResponseEntity<ListaTrabajoFiltros> findFiltros(@RequestBody(required = false) String qparams) throws CustomException{
        return new ResponseEntity<> (iListaDeTrabajoService.findListaTrabajoFilters(qparams), HttpStatus.OK);
    }
    
    @PostMapping("/findResponsables")
    public List<Usuario> findResponsables(@RequestBody(required = false) String qparams) throws CustomException{
        return iListaDeTrabajoService.findResponsables(qparams);
    }
    
    @PostMapping("/findTareasPendientes")
    public TareasPendientesResponse findTareasPendientes(@RequestBody(required = false) String qparams) throws CustomException{
        return iListaDeTrabajoService.findTareasPendientes(qparams);
    }
    
    @PostMapping("/validarTieneFormulario")
    public Boolean validarTieneFormulario(@RequestBody(required = false) String qparams) throws CustomException{
        return iListaDeTrabajoService.validarTieneFormulario(qparams);
    }
    
    @PostMapping("/cargarTareasAsignadas")
    public List<TareasAsignadasResponseDTO> cargarTareasAsignadas(@RequestBody(required = false) String qparams) throws CustomException{
        return iListaDeTrabajoService.cargarTareasAsignadas(qparams);
    }

}
