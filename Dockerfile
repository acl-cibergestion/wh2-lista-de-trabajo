# FROM gradle:6.5.1-jdk8 AS build
# COPY --chown=gradle:gradle . /home/gradle/src
# WORKDIR /home/gradle/src
# # install node
# ENV NODE_VERSION=12.6.0
# RUN curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.34.0/install.sh | bash
# ENV NVM_DIR=/root/.nvm
# RUN . "$NVM_DIR/nvm.sh" && nvm install ${NODE_VERSION}
# RUN . "$NVM_DIR/nvm.sh" && nvm use v${NODE_VERSION}
# RUN . "$NVM_DIR/nvm.sh" && nvm alias default v${NODE_VERSION}
# ENV PATH="/root/.nvm/versions/node/v${NODE_VERSION}/bin/:${PATH}"

# # compile project
# RUN gradle build -PenvironmentName=prod --no-daemon

# # run app
# FROM openjdk:8-jdk-alpine
# EXPOSE 8080
# RUN mkdir /app
# COPY --from=build /home/gradle/src/build/libs/*.jar /app/spring-boot-application.jar

FROM maven:3.6.3-jdk-11-slim AS maven_builder

COPY pom.xml /tmp/
RUN mvn -B dependency:go-offline -f /tmp/pom.xml -s /usr/share/maven/ref/settings-docker.xml
COPY . /tmp
WORKDIR /tmp/
RUN mvn -B -s /usr/share/maven/ref/settings-docker.xml package

# run app
FROM openjdk:11-jre-slim
EXPOSE 8080
RUN mkdir /app
# COPY --from=build /home/gradle/src/build/libs/*.jar /app/spring-boot-application.jar
COPY --from=maven_builder /tmp/target/*.jar /app/spring-boot-application.jar
ENTRYPOINT ["java", "-server", "-XX:+UseParallelGC", "-XX:+UseNUMA", "-Djava.security.egd=file:/dev/./urandom","-jar","/app/spring-boot-application.jar"]

